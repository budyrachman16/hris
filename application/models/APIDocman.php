<?php  
class APIDocman extends CI_Model{


    function __construct(){
        parent::__construct();
        $this->authorization = $this->config->item('core-authorization');
        // $this->url  = $this->config->item('url-corelogbook');
        $this->url = $this->config->item('url');
        

    }

    function Apiget($url){
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }


    function getDocByUser($url){
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        $response = json_decode($json_response);
        return $json_response;

    }

    function Apidel($url) {
        $header = array (
            "Authorization: $this->authorization",
            "content-type:application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }

    function uploadFile($id_project, $entity, $tmpFile, $typeFile, $nameFile) {

        $arr = array('file'=> new CURLFile($tmpFile,$typeFile,$nameFile),'entity' => $entity,'id_project' => $id_project);
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $this->url."/doc/upload/file",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $arr,
        CURLOPT_HTTPHEADER => array(
            "appsession: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0"
        ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response);

        return $response;
    }


    function inputDokumen($url, $body) {


        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->url."/doc/post",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS =>"$body",
          CURLOPT_HTTPHEADER => array(
            "appsession: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0",
            "Content-Type: application/json"
          ),
        ));
        
        $response = curl_exec($curl);

    }


    // config file
    function ApigetTipe($url){
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }


    function ApiPostTipe($url, $body) {
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $arr=json_encode($body);
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $arr);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }

    function ApidelType($url) {
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }


    function Apiput($url, $body) {
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $arr = json_encode($body); 
        $curl = curl_init($url); 
        curl_setopt($curl, CURLOPT_HEADER, false); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT"); 
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header); 
        curl_setopt($curl, CURLOPT_POST, true); 
        curl_setopt($curl, CURLOPT_POSTFIELDS, $arr); 
        $json_response = curl_exec($curl); 
        curl_close($curl); 
        return $json_response; 
    }


    function linkDokumen(){
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $arr = json_encode($body); 
        $curl = curl_init($url); 
        curl_setopt($curl, CURLOPT_HEADER, false); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST"); 
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header); 
        curl_setopt($curl, CURLOPT_POST, true); 
        curl_setopt($curl, CURLOPT_POSTFIELDS, $arr); 
        $json_response = curl_exec($curl); 
        curl_close($curl); 
        return $json_response; 

    }


    function getPartner($url){
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }


    function getPartnerType($url){
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }

    function getDocType($url){
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }


    function postDoctype($url, $body) {
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $arr=json_encode($body);
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $arr);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }

    function apiDeletedoctype($url) {
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }


    function ApiputDoctype($url, $body) {
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $arr = json_encode($body); 
        $curl = curl_init($url); 
        curl_setopt($curl, CURLOPT_HEADER, false); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT"); 
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header); 
        curl_setopt($curl, CURLOPT_POST, true); 
        curl_setopt($curl, CURLOPT_POSTFIELDS, $arr); 
        $json_response = curl_exec($curl); 
        curl_close($curl); 
        return $json_response; 
    }



    function getpartnerAll($url){
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }

    function getpartnerTypeAll($url){
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }


    function postPartner($url, $body) {
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $arr=json_encode($body);
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $arr);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }

    function postPartnerType($url, $body) {
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $arr=json_encode($body);
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $arr);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }


    function ApiDelPartner($url) {
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }


    function ApiDelPartnerType($url) {
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }


    function ApigetEmailDoc($url){
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }

    function ApigetEmail($url){
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }


    function sendEmail($url, $body) {
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $arr=json_encode($body);
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $arr);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }


    function ApiputPartner($url, $body) {
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $arr = json_encode($body); 
        $curl = curl_init($url); 
        curl_setopt($curl, CURLOPT_HEADER, false); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT"); 
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header); 
        curl_setopt($curl, CURLOPT_POST, true); 
        curl_setopt($curl, CURLOPT_POSTFIELDS, $arr); 
        $json_response = curl_exec($curl); 
        curl_close($curl); 
        return $json_response; 
    }



    function ApiUploadSupport($url, $body) {
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $arr=json_encode($body);
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $arr);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }

    function ApiUploadNewVer($url, $body) {
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $arr=json_encode($body);
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $arr);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }

    function getDocChart($url){
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }


    function getDocChartId($url){
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }


    function getDotypeList($url){
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }

    function getPartnerList($url){
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }


    function getChartPartner($url){
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }


    function getDocChartYear($url){
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }

    function getDocTypeChart($url){
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }

    function getDocByDate($url){
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }

    
    function getChartByDocPartDate($url){
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }


    function getViewDoc($url){
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }


    function getLinkFile($url){
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }



    function getSupportdoc($url){
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }

    function LinkDoc($url, $body){
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }




    function ApiDelReference($url, $body) {
   

        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0',
            "Content-type: application/json"
        );
        $arr = json_encode($body); 
        $curl = curl_init($url); 

        // var_dump($arr);
        // var_dump($url);
        // exit;

        curl_setopt($curl, CURLOPT_HEADER, false); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT"); 
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header); 
        curl_setopt($curl, CURLOPT_POST, true); 
        curl_setopt($curl, CURLOPT_POSTFIELDS, $arr); 
        $json_response = curl_exec($curl); 
        curl_close($curl); 
        return $json_response; 
    }




}; 

?>