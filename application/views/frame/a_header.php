<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    
    <title>Houris</title>
    
    <link rel="apple-touch-icon" href="<?php echo base_url()."images/home.ico"?>">
    <link rel="shortcut icon" href="<?php echo base_url()."images/favicon.ico"?>">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="<?php echo base_url(). "assets/"?>css/site.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/animsition/animsition.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/switchery/switchery.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/waves/waves.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/jquery-selective/jquery-selective.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/bootstrap-datepicker/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/asscrollable/asScrollable.css">
        <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/jvectormap/jquery-jvectormap.css">

        <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/jquery-selective/jquery-selective.css">

        <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/datatables.net-bs4/dataTables.bootstrap4.css">
        <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/datatables.net-fixedheader-bs4/dataTables.fixedheader.bootstrap4.css">
        <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/datatables.net-fixedcolumns-bs4/dataTables.fixedcolumns.bootstrap4.css">
        <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/datatables.net-rowgroup-bs4/dataTables.rowgroup.bootstrap4.css">
        <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/datatables.net-scroller-bs4/dataTables.scroller.bootstrap4.css">
        <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/datatables.net-select-bs4/dataTables.select.bootstrap4.css">
        <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css">
        <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/datatables.net-buttons-bs4/dataTables.buttons.bootstrap4.css">
        <link rel="stylesheet" href="<?php echo base_url(). "assets/"?>examples/css/tables/datatable.css">
        <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/jquery-wizard/jquery-wizard.css">
        <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/formvalidation/formValidation.css">
        <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/footable/footable.core.css">
        <link rel="stylesheet" href="<?php echo base_url(). "assets/"?>examples/css/tables/footable.css">
   
        
    
        
    

    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/tasklist/tasklist.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="<?php echo base_url(). "assets/" ?>examples/css/widgets/data.css">

    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/select2/select2.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/bootstrap-tokenfield/bootstrap-tokenfield.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/bootstrap-tagsinput/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/bootstrap-select/bootstrap-select.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/icheck/icheck.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/switchery/switchery.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/asrange/asRange.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/ionrangeslider/ionrangeslider.min.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/asspinner/asSpinner.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/clockpicker/clockpicker.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/timepicker/jquery-timepicker.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/ascolorpicker/asColorPicker.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/bootstrap-touchspin/bootstrap-touchspin.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/jquery-labelauty/jquery-labelauty.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/bootstrap-datepicker/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/bootstrap-maxlength/bootstrap-maxlength.css">
    
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/jquery-strength/jquery-strength.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/multi-select/multi-select.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/typeahead-js/typeahead.css">
    <link rel="stylesheet" href="<?php echo base_url(). "assets/" ?>examples/css/forms/advanced.css">

      


    
    


    <!-- Scripts -->
    <script src="<?php echo base_url(). "global/" ?>vendor/jquery/jquery.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/breakpoints/breakpoints.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/formvalidation/formValidation.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/formvalidation/framework/bootstrap.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/matchheight/jquery.matchHeight-min.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/jquery-wizard/jquery-wizard.js"></script>

    
    
    
    <!-- Fonts -->
    <link rel="stylesheet" href="<?php echo base_url(). "global/"?>fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/"?>fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>


    <script>
      Breakpoints();
    </script>
  </head>