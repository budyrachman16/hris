<div class="page">

<!-- Header Page -->
  <!-- Header Page -->

  <!-- Content Page -->
  <div class="page-content">
      <h1 class="page-title">Level List</h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo site_url('home')?>">Home</a></li>
          <li class="breadcrumb-item active">Level</li>
        </ol>

    <!-- Body Page -->
    <div class="panel">
      <header class="panel-heading">
          <h3 class="panel-title">
          <br>
            <button id="add" class="btn btn-primary" type="button" data-target="#modal_create"
                data-toggle="modal">
              <i class="icon md-account-add" aria-hidden="true"></i> Add Level
            </button>
        </h3>
      </header>
      <div class="panel-body collapse show" id="exampleFooCollapsePanel">
        <table class="table table-bordered table-hover toggle-circle" id="exampleFooCollapse"
          data-paging="true" data-filtering="true" data-sorting="true">
          <thead>
            <tr>
              <th data-name="id" data-type="number" data-breakpoints="xs" width=20px>#</th>
              <th data-name="firstName" width=200px>Level Name</th>
              <th data-name="lastName" width=200px>Action</th>           
            </tr>
          </thead>   
          <tbody> 
              <?php $i = 1; foreach ($level as $le): ?>
                <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?= $le->name ?></td>
                
                    <td class="actions">
                      <!-- Edit FIle -->
                      <a href="#" onClick="return edit_config('<?php echo $le->id_level; ?>','<?php echo $le->name; ?>','<?php echo $le->company;?>')"  class="btn btn-info btn-xs" data-toggle="modal" data-target="#modal_edit"><i class="icon md-edit"></i> Edit </a>
                      <!-- Delete -->
                      <!-- <a href="#" onclick='return del_confirm()' class="btn btn-danger btn-xs"><i class="icon md-delete"></i> Delete </a>  -->
                      <!-- <detail> -->
                    </td>    
                </tr>                      
            <?php endforeach; ?>
          </tbody>            
        </table>
      </div>
    </div>



  </div>
    <div class="modal fade" id="modal_create" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myModalLabel">Add Level</h3>
            </div>
          <!-- <div class="modal-header"></div> -->
          <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('level/postlevel')?>" onsubmit="disableButton()">
            <div class="modal-body">             
                <div class="form-group">
                    <label class="control-label col-xs-3">Level</label>
                    <div class="col-xs-8">
                        <input name="add_level" id='add_level' class="form-control" type="text" >
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-info" type="submit">Input</button>
                </div>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="myModalLabel">Edit Level</h3>
                </div>
                  <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('level/editlevel')?>" onsubmit="disableButton()">
                      <div class="modal-body">
                          <div class="form-group">
                                  <input name="id_level" id='id_level' class="form-control" type="text" hidden>
                              <label class="control-label col-xs-3">Level</label>
                              <div class="col-xs-8">
                                  <input name="edit_level" id='edit_level' class="form-control" type="text" >
                              </div>
                              <div class="modal-footer">
                                  <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                                  <button class="btn btn-info" type="submit">Edit</button>
                              </div>
                          </div>
                      </div>
                  </form>
            </div>
        </div>
    </div>
</div>



























<script type="text/javascript">
  
  function upload(){

    let upload_file = document.getElementById("upload_file").value;
    let nama_c = document.getElementById("nama_c").value;
    let nama_file = document.getElementById("nama_file").value;
    let input_file = document.getElementById("input_file").value;
    let file_type = document.getElementById("file_type").value;
    

    project = $.ajax({
            data : { id_project : id_project },
            type:"POST",
            url: "<?php echo site_url('dokumen/getProject');?>",
            async: false
      }).responseText;
   
  }


  function edit_config(id_level,name,company){
    document.getElementById("id_level").value = id_level
    document.getElementById("edit_level").value = name;
    document.getElementById("file_ext_e").value = file_ext_e;
  }

  function disableButton() {
    var btn = document.getElementById('btn_add_submit');
    btn.disabled = true;
    btn.innerText = 'Posting...';
}
</script>
<!--END MODAL EDIT-->
  
    
    
