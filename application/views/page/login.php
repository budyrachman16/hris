

<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    
    <title>Login V3 | Remark Material Admin Template</title>
    
       
    <link rel="apple-touch-icon" href="<?php echo base_url()."images/home.ico"?>">
    <link rel="shortcut icon" href="<?php echo base_url()."images/favicon.ico"?>">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="<?php echo base_url(). "assets/" ?>css/site.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/animsition/animsition.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/switchery/switchery.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>vendor/waves/waves.css">
        <link rel="stylesheet" href="<?php echo base_url(). "assets/" ?>examples/css/pages/login-v3.css">
    
    
    <!-- Fonts -->
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="<?php echo base_url(). "global/" ?>fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    
    <!--[if lt IE 9]>
    <script src="../../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="../../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="<?php echo base_url(). "global/" ?>vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
  </head>
  <body class="animsition page-login-v3 layout-full">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->


    <!-- Page -->
    <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">>
      <div class="page-content vertical-align-middle">
        <div class="panel">
          <div class="panel-body">
            <div class="brand">
              <img class="brand-img" src="<?php echo base_url(). "assets/" ?>images/logo-colored.png" alt="...">
              <h2 class="brand-text font-size-18">Hris Web</h2>
              <!-- <h2 class="brand-text font-size-18">Login</h2> -->
            </div>
            <?php echo form_open_multipart('/login/user_login'); ?>
              <div class="form-group form-material floating" data-plugin="formMaterial">
                <input type="email" class="form-control" name="email" />
                <label class="floating-label">Email</label>
              </div>
              <div class="form-group form-material floating" data-plugin="formMaterial">
                <input type="password" class="form-control" name="password" />
                <label class="floating-label">Password</label>
              </div>
              <div class="form-group clearfix">
                <div class="checkbox-custom checkbox-inline checkbox-primary checkbox-lg float-left">
                  <input type="checkbox" id="inputCheckbox" name="remember">
                  <label for="inputCheckbox">Remember me</label>
                </div>
                <a class="float-right" href="forgot-password.html">Forgot password?</a>
              </div>
              <button type="submit" class="btn btn-primary btn-block">Sign in</button>
              <?php echo form_close(); ?>
            </form>
            <p>Still no account? Please go to <a href="register-v3.html">Sign up</a></p>
          </div>
        </div>

    <div class="modal fade modal-danger" id="warningmodal" aria-hidden="true" aria-labelledby="modalWarning" role="dialog" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title">Application Warning</h4>
          </div>
          <div class="modal-body">
            <h5><?php echo json_decode(json_encode($this->session->flashdata('message')))->message; ?></h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade modal-success" id="successmodal" aria-hidden="true" aria-labelledby="modalSuccess" role="dialog" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title">Application Success</h4>
          </div>
          <div class="modal-body">
            <h5><?php echo json_decode(json_encode($this->session->flashdata('message')))->message; ?></h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

        <footer class="page-copyright page-copyright-inverse">
          <p>WEBSITE BY Product Development</p>
          <p>© 2019. All RIGHT RESERVED.</p>
          <p>PT. Graha Informatika Nusantara</p>
        </footer>
      </div>
    </div>
    <!-- End Page -->


    <!-- Core  -->
    <script src="<?php echo base_url(). "global/" ?>vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/jquery/jquery.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/popper-js/umd/popper.min.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/bootstrap/bootstrap.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/animsition/animsition.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/waves/waves.js"></script>
    
    <!-- Plugins -->
    <script src="<?php echo base_url(). "global/" ?>vendor/switchery/switchery.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/intro-js/intro.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/screenfull/screenfull.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/jquery-placeholder/jquery.placeholder.js"></script>
    
    <!-- Scripts -->
    <script src="<?php echo base_url(). "global/" ?>js/Component.js"></script>
    <script src="<?php echo base_url(). "global/" ?>js/Plugin.js"></script>
    <script src="<?php echo base_url(). "global/" ?>js/Base.js"></script>
    <script src="<?php echo base_url(). "global/" ?>js/Config.js"></script>
    
    <script src="<?php echo base_url(). "assets/" ?>js/Section/Menubar.js"></script>
    <script src="<?php echo base_url(). "assets/" ?>js/Section/Sidebar.js"></script>
    <script src="<?php echo base_url(). "assets/" ?>js/Section/PageAside.js"></script>
    <script src="<?php echo base_url(). "assets/" ?>js/Plugin/menu.js"></script>
    
  
    
    <!-- Page -->
    <script src="<?php echo base_url(). "assets/" ?>js/Site.js"></script>
    <script src="<?php echo base_url(). "global/" ?>js/Plugin/asscrollable.js"></script>
    <script src="<?php echo base_url(). "global/" ?>js/Plugin/slidepanel.js"></script>
    <script src="<?php echo base_url(). "global/" ?>js/Plugin/switchery.js"></script>
        <script src="<?php echo base_url(). "global/" ?>js/Plugin/jquery-placeholder.js"></script>
        <script src="<?php echo base_url(). "global/" ?>js/Plugin/material.js"></script>
    
    <script>
      (function(document, window, $){
        'use strict';
    
        var Site = window.Site;
        $(document).ready(function(){
          Site.run();
        });
      })(document, window, jQuery);
    </script>
    
    <?php if($this->session->flashdata('message') != NULL) {
      $status = json_decode(json_encode($this->session->flashdata('message')))->status;
      if($status == '1'){ ?>
        <script type="text/javascript"> $(window).on('load',function(){ $('#successmodal').modal('show'); }); </script>
     <?php } else { ?>
        <script type="text/javascript"> $(window).on('load',function(){ $('#warningmodal').modal('show'); }); </script>
    <?php }} ?>
  
  </body>
</html>
