<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{
    public function verifyLoginPassword($username,$password) {

        // $hash = hash('sha256', $password);
        $qry = $this->db->query("select * from tb_role where nik = '$username'");
   
        if($qry->num_rows() > 0) {
            foreach($qry->result() as $d) :
                $data = $d;
            endforeach;

            if($password == $data->password) {
                return $data;
            }
        }
    }

    public function getAll()
    {   
        $query = $this->db->query(" select * from tb_user_logbook ");
        foreach($query->result() as $d) {
            $data[] = $d;
        }  
        return $data;
    }
    
    public function getByUsername($username)
    {
        $query = $this->db->query(" select * from tb_user_logbook where username='$username'");
        foreach($query->result() as $d) {
            $data = $d;
        }  
        return $data;
    }

    public function save($username,$password,$level)
    {
        $date = date('Y-m-d H:i:s'); 
        $hash = hash('sha256', $password);
        $this->db->query(" insert into tb_user_logbook (
            `username`,
            `password`,
            `status`,
            `level`,
            `login`,
            `last_login`
        ) values (
            '$username',
            '$hash',
            '1',
            '$level',
            '0',
            '$date'
        ) ");
    }

    public function update($username,$password,$status,$level)
    {   
        $date = date('Y-m-d H:i:s');
        $hash = hash('sha256', $password);
        $this->db->query(" 
        update tb_user_logbook set password = '$hash',
                        status = '$status',
                        level = '$level',
                        last_login = '$date'
        where username = '$username' 
        ");
    }

    public function updatePassword($username,$password)
    {   
        $date = date('Y-m-d H:i:s');
        $hash = hash('sha256', $password);
        $this->db->query(" 
        update tb_user_logbook set password = '$hash',
                                last_login = '$date'
        where username = '$username' 
        ");
    }

    public function delete($username)
    {
        $this->db->query(" delete from tb_user_logbook where username='$username'");
    }

    public function cek_username($username) {
        $q = $this->db->query(" select * from tb_user_logbook where username = '$username' ");
        if($q->num_rows() > 0) {
            return false;
        }
        else {
            return true;
        }
    }

    public function checkOld($username, $pass) {
        $hash = hash('sha256',$pass);
        $q = " select * from tb_user_logbook where username = '$username' and password = '$hash' ";
        $qry = $this->db->query($q);
        if( $qry->num_rows() > 0 ) {
            return true; 
        }
        else {
            return false;
        }
    }

    public function login_out($username,$log)
    {   
        $date = date('Y-m-d H:i:s');
        $this->db->query(" 
        update tb_user_logbook set login = '$log',
                             last_login = '$date'
        where username = '$username' 
        ");
    }

    public function status($username,$status)
    {
        $date = date('Y-m-d H:i:s');
        $this->db->query(" 
        update tb_user_logbook set status = '$status',
                                last_login = '$date'
        where username = '$username' 
        ");
    }
}