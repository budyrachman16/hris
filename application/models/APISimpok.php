<?php
class APISimpok extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->url = $this->config->item('url-simpok');
    }

    function getLogin($username,$password){
      $url = $this->url."/1/$username/$password/0/350ea7e2af60c9d3824791dd122272d8";
	    $curl=curl_init($url);
      curl_setopt($curl, CURLOPT_HEADER, false);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      $json_response=curl_exec($curl);
      curl_close($curl);
      return $json_response;
    }

    
    function getAllPegawai(){
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "http://www.gratika.co.id/apigrtk/index.php/web_service/api/pegawai/0/0/0/0",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => array('apiKey' => '350ea7e2af60c9d3824791dd122272d8','access' => 'G01415','apicode' => '2','nik' => ''),
        CURLOPT_HTTPHEADER => array(
          
        ),
      ));
      
      $response = curl_exec($curl);
      
      curl_close($curl);
      return $response;
      }
  
      function getAllPegawaiId($id){
        $url = $this->url."/pegawai/id";
        $data = array(
          'apiKey' => '350ea7e2af60c9d3824791dd122272d8',
          'access' => 'G16001K',
          'apicode' => '2',
          'id' => $id
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
      }
  
      function getAllPegawaiNik(){
        $url = $this->url."/pegawai/nik";
        $data = array(
          'apiKey' => '350ea7e2af60c9d3824791dd122272d8',
          'access' => 'G16001K',
          'apicode' => '2',
          'nik' => 'G16001K'
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
      }

      // function getAllDivisi(){
      //   $url = $this->url."/divisi/0/0/0/0";
      //   $data = array(
      //     'apiKey' => '350ea7e2af60c9d3824791dd122272d8',
      //     'access' => 'G16001K',
      //     'apicode' => '2',
      //     'id' => ''
      //   );
      //   $curl=curl_init($url);
      //   curl_setopt($curl, CURLOPT_HEADER, false);
      //   curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      //   curl_setopt($curl, CURLOPT_POST, true);
      //   curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
      //   $json_response=curl_exec($curl);
      //   curl_close($curl);
      //   return $json_response;
      // }

      function getSubdivisi($id){
        $url = $this->url."/divisi/subdivisi/0/0/0";
        $data = array(
          'apiKey' => '350ea7e2af60c9d3824791dd122272d8',
          'access' => 'G16001K',
          'apicode' => '2',
          'id' => $id
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
      }
}