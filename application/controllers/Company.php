<?php

class company extends CI_Controller{

    function __construct(){

        parent::__construct();
        
        // $this->load->helper('url');
	      $this->load->helper('form');
        $this->load->library('session');
        $this->load->model("user_model");
        $this->load->model("APIDocman");
        $this->load->model("APILogbook");
        $this->url_hris = $this->config->item('url-hris');
        $this->load->model("APISimpok");        
        $this->url = $this->config->item('url');
        $this->load->model('Model');
        $this->session_key = $this->config->item('session-key');

    }

    function index(){


    
      $id_company = $this->session->userdata('id');
            

      $company = json_decode($this->APIDocman->ApigetTipe($this->url_hris."/company/$id_company"))->data;

      // $company = json_encode($company);

      $provisinsi = $company[0]->prov->nama;

      // echo $provisinsi;
      // exit;

      



      
      $data["id_company"] = $id_company;
      $data["company"] = $company;
      $data["provisinsi"] = $provisinsi;




            // var_dump($data);
        $key = $this->session->userdata('log');

        if ($key =="loveyou"){
            $this->load->view('frame/a_header');
            $this->load->view('frame/b_nav');
            $this->load->view('page/company', $data);
            $this->load->view('frame/d_footer');    
        }else{
            $this->load->view('page/login');
        }
    }

    


    function postPartner(){

        $partner = $this->input->post('add_partner');

          $body = array(
            "partner" =>  $partner
        );

        $respon = json_decode($this->APIDocman->postPartner($this->url."/partner/post", $body));

        if($respon==null){
          $array=array('status' => '0','message' => 'API not response . please contact administrator');
          $this->session->set_flashdata('message', $array);
        } else if($respon->status =='1'){
          $array=array('status' => '1','message' => 'Data has been Added.. ');
          $this->session->set_flashdata('message', $array);
        } else {
          $array=array('status' => '0','message' => $respon->data);
          $this->session->set_flashdata('message', $array);
        }
        redirect('partner');
    }



    function deletePartner($id){
      $respon = json_decode($this->APIDocman->ApiDelPartner($this->url."/partner/delete/$id"));

      // var_dump($respon);
      // exit;
      if($respon==null){
        $array=array('status' => '0','message' => 'API not response . please contact administrator');
        $this->session->set_flashdata('message', $array);
      } else if($respon->status =='1'){
        $array=array('status' => '0','message' => 'Data has been Deleted.. ');
        $this->session->set_flashdata('message', $array);
      } else {
        $array=array('status' => '0','message' => $respon->data);
        $this->session->set_flashdata('message', $array);
      }
      redirect('partner');
    }


    function edit_partner(){
        $id = $this->input->post('id');
        $partner = $this->input->post('partner');
   

        $body = array(
            "partner" =>  $partner,
        );
       
     

        $respon = json_decode($this->APIDocman->ApiputPartner($this->url."/partner/update/$id", $body));

        // var_dump($respon);
        // exit;

        if($respon==null){
          $array=array('status' => '0','message' => 'API not response . please contact administrator');
          $this->session->set_flashdata('message', $array);
        } else if($respon->status =='1'){
          $array=array('status' => '1','message' => 'Data has been Edited.. ');
          $this->session->set_flashdata('message', $array);
        } else {
          $array=array('status' => '0','message' => $respon->data);
          $this->session->set_flashdata('message', $array);
        }
        redirect('partner');
      }


   

} 
?>