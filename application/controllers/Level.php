<?php

class level extends CI_Controller{

    function __construct(){

        parent::__construct();
        
        // $this->load->helper('url');
	    $this->load->helper('form');
        $this->load->library('session');
        $this->load->model("user_model");
        $this->load->model("APIDocman");
        $this->load->model("APILogbook");
        $this->load->model("APISimpok");        
        $this->load->model("APIHris");        
        $this->url_hris = $this->config->item('url-hris');
        $this->load->model('Model');
        $this->session_key = $this->config->item('session-key');

    }

    function index(){

        $id_company = $this->session->userdata('id');


        $level = json_decode($this->APIHris->Apiget($this->url_hris."/level/company/$id_company"))->data;


        $data["level"] = $level;


        // var_dump($level);
        // exit;


        $key = $this->session->userdata('log');

        if ($key =="loveyou"){
            $this->load->view('frame/a_header');
            $this->load->view('frame/b_nav');
            $this->load->view('page/level',$data);
            $this->load->view('frame/d_footer');    
        }else{
            $this->load->view('page/login');
        }
    }


    function postlevel(){

        $id_company = $this->session->userdata('id');
        
  
        
        $level = $this->input->post('add_level');

        
        $body = array(
          "name" => $level,
          "id_company" => $id_company,
    
      );
  
      $respon = json_decode($this->APIHris->ApiPost($this->url_hris."/level", $body));
  
      // var_dump($respon);
      // exit;
  
        if($respon==null){
          $array=array('status' => '0','message' => 'API not response . please contact administrator');
          $this->session->set_flashdata('message', $array);
        } else if($respon->status =='1'){
          $array=array('status' => '1','message' => 'Data has been Added.. ');
          $this->session->set_flashdata('message', $array);
        } else {
          $array=array('status' => '0','message' => $respon->data);
          $this->session->set_flashdata('message', $array);
        }
        redirect('level');
    }

    


    function editlevel(){
        $id_company = $this->session->userdata('id');


        $id_level = $this->input->post('id_level');

        $level = $this->input->post('edit_level');

        // var_dump($id_level);
        // exit;

        $body = array(
            "name" =>  $level,
        );


        



       
        $respon = json_decode($this->APIDocman->Apiput($this->url_hris."/level/$id_level", $body));

        // var_dump($respon);
        // exit;


       


        if($respon==null){
          $array=array('status' => '0','message' => 'API not response . please contact administrator');
          $this->session->set_flashdata('message', $array);
        } else if($respon->status =='1'){
          $array=array('status' => '1','message' => 'Data has been Edited.. ');
          $this->session->set_flashdata('message', $array);
        } else {
          $array=array('status' => '0','message' => $respon->data);
          $this->session->set_flashdata('message', $array);
        }
        redirect('level');
      }




} 
?>