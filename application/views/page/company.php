<div class="page">

<!-- Header Page -->

  <div class="page-header">
    <div class="page-header">
      <h1 class="page-title">Company</h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo site_url('home')?>">Home</a></li>
          <li class="breadcrumb-item active">Company</li>
        </ol>
        <div id="example1"></div>
    </div>
  </div>
  <!-- Header Page -->

  <!-- Content Page -->
  <div class="page-content">
      <div class="row">
        <div class="col-md-6">
            <div class="mb-15">
                <button id="add" class="btn btn-outline btn-primary" onClick="modal_create()"  type="button" data-target="#modal_create"
                data-toggle="modal">
                    <i class="icon md-account-add" aria-hidden="true"></i> Add Company
                </button>
            </div>
        </div>
      </div>
    <!-- Body Page -->
    <div class="panel">
      <div class="panel-body">
        <div class="example-wrap">
            <div class="example table-responsive">
              <table class="table table-hover dataTable table-striped w-full" cellspacing="0" data-plugin="dataTable">
                <thead>
                  <tr>
                    <th>#</th>                  
                    <th>Name</th>
                    <th>Alamat</th>
                    <th>Provinsi</th>
                    <th>Nomor</th>
                    <th>Email</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody> 
                  <?php $i = 1; foreach ($company as $com[0]): ?>
                      <tr>
                          <td><?php echo $i++; ?></td>
                          <td><?= $com[0]->name ?></td>
                          <td><?= $com[0]->alamat ?></td>                      
                          <td><?= $com[0]->prov->nama ?></td>                                          
                          <td><?= $com[0]->telephone?></td>                                          
                          <td><?= $com[0]->email ?></td>                                          
                          <td class="actions">
                            <!-- Edit FIle -->
                            <a href="#" onClick="return edit_config"  class="btn btn-info btn-xs" data-toggle="modal" data-target="#modal_edit"><i class="icon md-edit"></i> Edit </a>
                            <!-- Delete -->
                            <a href="#" onclick='return del_confirm()' class="btn btn-danger btn-xs"><i class="icon md-delete"></i> Delete </a> 
                            <!-- <detail> -->
                            <a href="#" onclick='return del_confirm()' class="btn btn-success btn-xs"><i class="icon md-receipt"></i> Detail </a> 
                          </td>    
                      </tr>                      
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>



























<script type="text/javascript">
  
  function upload(){

    let upload_file = document.getElementById("upload_file").value;
    let nama_c = document.getElementById("nama_c").value;
    let nama_file = document.getElementById("nama_file").value;
    let input_file = document.getElementById("input_file").value;
    let file_type = document.getElementById("file_type").value;
    

    project = $.ajax({
            data : { id_project : id_project },
            type:"POST",
            url: "<?php echo site_url('dokumen/getProject');?>",
            async: false
      }).responseText;
   
  }


  function edit_config(id_type,type_file_e,file_ext_e){
    document.getElementById("id_type").value = id_type
    document.getElementById("type_file_e").value = type_file_e;
    document.getElementById("file_ext_e").value = file_ext_e;
  }

  function disableButton() {
    var btn = document.getElementById('btn_add_submit');
    btn.disabled = true;
    btn.innerText = 'Posting...';
}
</script>
<!--END MODAL EDIT-->
  
    
    
