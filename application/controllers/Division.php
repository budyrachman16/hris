<?php

class division extends CI_Controller{

    function __construct(){

        parent::__construct();
        
        // $this->load->helper('url');
	    $this->load->helper('form');
        $this->load->library('session');
        $this->load->model("user_model");
        $this->load->model("APIDocman");
        $this->load->model("APILogbook");
        $this->load->model("APISimpok");        
        $this->load->model("APIHris");        
        $this->url_hris = $this->config->item('url-hris');
        $this->url = $this->config->item('url');
        $this->load->model('Model');
        $this->session_key = $this->config->item('session-key');

    }

    function index(){


      $id_company = $this->session->userdata('id');

        $divisi = json_decode($this->APIHris->Apiget($this->url_hris."/divisi/company/$id_company"))->data;


        $data["divisi"] = $divisi;


        // var_dump($divisi);
        // exit;
  


            // var_dump($data);
        $key = $this->session->userdata('log');

        if ($key =="loveyou"){
            $this->load->view('frame/a_header');
            $this->load->view('frame/b_nav');
            $this->load->view('page/division', $data);
            $this->load->view('frame/d_footer');    
        }else{
            $this->load->view('page/login');
        }
    }

    


    function postDivision(){

      $id_company = $this->session->userdata('id');  
      $division = $this->input->post('add_division');

      
      $body = array(
        "name" => $division,
        "id_company" => $id_company,
  
      );

      $respon = json_decode($this->APIHris->ApiPost($this->url_hris."/divisi", $body));


      if($respon==null){
        $array=array('status' => '0','message' => 'API not response . please contact administrator');
        $this->session->set_flashdata('message', $array);
      } else if($respon->status =='1'){
        $array=array('status' => '1','message' => 'Data has been Added.. ');
        $this->session->set_flashdata('message', $array);
      } else {
        $array=array('status' => '0','message' => $respon->data);
        $this->session->set_flashdata('message', $array);
      }
      redirect('division');
    }


    function editDivision(){
      $id_company = $this->session->userdata('id');


      $id_division = $this->input->post('id_division');

      $edit_division = $this->input->post('edit_division');


      $body = array(
          "name" =>  $edit_division,
      );


         
      // var_dump($body);
      // exit;

      



     
      $respon = json_decode($this->APIDocman->Apiput($this->url_hris."/divisi/$id_division", $body));

    

      if($respon==null){
        $array=array('status' => '0','message' => 'API not response . please contact administrator');
        $this->session->set_flashdata('message', $array);
      } else if($respon->status =='1'){
        $array=array('status' => '1','message' => 'Data has been Edited.. ');
        $this->session->set_flashdata('message', $array);
      } else {
        $array=array('status' => '0','message' => $respon->data);
        $this->session->set_flashdata('message', $array);
      }
      redirect('division');
    }
   

} 
?>