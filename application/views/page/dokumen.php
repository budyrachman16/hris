<div class="page">




<!-- Header Page -->

  <div class="page-header">
    <h1 class="page-title">Data Document</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url('home')?>">Home</a></li>
        <li class="breadcrumb-item active">Document</li>
      </ol>
      <div id="example1"></div>
  </div>
<!-- Header Page -->

  <!-- Content Page -->
  <div class="page-content">
    
    <!-- Body Page -->
    <div class="panel">
        <div class="panel-body">

      <!-- input data -->
          <div class="row">
              <div class="col-md-6">
                <div class="mb-15">
                  <?php if($this->session->userdata('status') == 'Admin') { ?>
                    <!-- <button id="add" class="btn btn-outline btn-primary" onClick="modal_create()"  type="button" data-target="#modal_create"
                    data-toggle="modal">
                      <i class="icon wb-add-file" aria-hidden="true"></i> Upload Document
                    </button> -->
                     <?php } ?>
                </div>
              </div>
          </div>
    <!-- input data -->


        <!-- Table -->
        <div class="example-wrap">
          <div class="example table-responsive">
            <table class="table table-hover dataTable table-striped w-full" cellspacing="8" data-plugin="dataTable">
              <thead>
                <tr>
                  <th>#</th>  
                  <th>Project Names </th>
                  <th>Doc Name</th>
                  <th>Uploaded By</th>
                  <th>Upload Date</th>
                  <th>Version</th>
                  <th>Info</th>
                </tr>
              </thead>
              <tbody> 
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


  <!-- Modal add -->
  <!-- end add -->




  




<script type="text/javascript">
  
  function upload(){
    let upload_file = document.getElementById("upload_file").value;
    let nama_c = document.getElementById("nama_c").value;
    let nama_file = document.getElementById("nama_file").value;
    let input_file = document.getElementById("input_file").value;
    let file_type = document.getElementById("file_type").value;
    
    project = $.ajax({
            data : { id_project : id_project },
            type:"POST",
            url: "<?php echo site_url('dokumen/getProject');?>",
            async: false
      }).responseText;
  }

    function revisi(id_user,id_docman,id_project,partner,partner_type,doc_type,start_date,end_date){
      document.getElementById("linkNewDoc").href  = "<?php echo site_url("Link/get/");?>"+id_docman; //linknewversion
      document.getElementById("linkNewDoc2").href  = "<?php echo site_url("Link/getSupport/");?>"+id_docman; //linksupporting
      document.getElementById("id_docman_r").value = id_docman;
      document.getElementById("id_user_r").value = id_user;
      document.getElementById("id_project_r").value = id_project;
      document.getElementById("partner_r").value = partner;
      document.getElementById("partner_typer").value = partner_type;
      document.getElementById("doc_typer").value = doc_type;
      document.getElementById("start_date_r").value = start_date;
      document.getElementById("end_date_r").value = end_date;
  }

  function send_email(id_docman,file_name,file_type,path_folder,add_sender_email,add_sender){

      console.log(id_docman);
      console.log(file_name);
      console.log(file_type);
      console.log(path_folder);

      document.getElementById("id_docman").value = id_docman;
      document.getElementById("add_sender_email").value = add_sender_email;
      document.getElementById("add_sender").value = add_sender;
      document.getElementById("subject").value = "Contract Document Management";
      document.getElementById("body").value = "Please use document as it needs and wisely";
      document.getElementById("file_name").value = file_name;
      document.getElementById("file_type").value = file_type;
      document.getElementById("path_folder").value = path_folder;
    
    riwayat = $.ajax({
            data: {
              id : id_docman
            },
            type: "POST",
            url: "<?php echo site_url('Dokumen/getEmailByD');?>",
 
 
            success: function(msg) {
              // message = msg;
              $("#dTable").html(msg);
              // $("#dTable").load(" #dTable *");
              $('#tblData').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": false,
                "bInfo": true,
                "bAutoWidth": true
              });
              console.log (msg);
            }
        });

  }


  function refreshTable() {
    $('#tabel_riwayat').DataTable().clear();
    $('#tabel_riwayat').DataTable().ajax.reload();
}


  function disableButton() {
    var btn = document.getElementById('btn_add_submit');
    btn.disabled = true;
    btn.innerText = 'Posting...';
}


function disableButtonEmail() {
    var btn = document.getElementById('btn_add_submitE');
    btn.disabled = true;
    btn.innerText = 'Sending...';
}


$(document).ready(function(){
        let id = document.getElementById("id_docman").value;
        // alert(id);
        $.post("<?php echo site_url('dokumen/getEmailByD');?>",
        {
          id : id_docman,
        },
        function(data,status){
            document.getElementById("table").innerHTML = data;
        });
    });

</script>
  
    
    
