<?php

class workhour extends CI_Controller{

    function __construct(){

        parent::__construct();
        
        // $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('download');
        $this->load->library('session');
        $this->load->model("user_model");
        $this->load->model("APIDocman");
        $this->load->model("APILogbook");
        $this->load->model("APISimpok");        
        $this->load->model("APIHris");        
        $this->url_hris = $this->config->item('url-hris');
        $this->url = $this->config->item('url');
        $this->load->model('Model');
        $this->session_key = $this->config->item('session-key');

    }

    function index(){


        $id_company = $this->session->userdata('id');
            

        $work_hour = json_decode($this->APIDocman->ApigetTipe($this->url_hris."/shift/company/$id_company"))->data;

        
        $data["id_company"] = $id_company;
        $data["work_hour"] = $work_hour;

            
        $key = $this->session->userdata('log');


        if ($key =="loveyou"){
            $this->load->view('frame/a_header');
            $this->load->view('frame/b_nav');
            $this->load->view('page/workhour',$data);
            $this->load->view('frame/d_footer');    
        }else{
            $this->load->view('page/login');
        }
    }



    function post_workhour(){

      $add_info = $this->input->post('add_info');  
      $start_time = $this->input->post('add_star_time');
      $end_time = $this->input->post('add_end_time');
      $id_company = $this->session->userdata('id');  


      
      $body = array(
        "start" => $start_time,
        "end" => $end_time,
        "id_company" => $id_company,
        "info" => $add_info
  
      );

        
    //   var_dump($body);
    //   exit;    


      $respon = json_decode($this->APIHris->ApiPost($this->url_hris."/shift", $body));


      if($respon==null){
        $array=array('status' => '0','message' => 'API not response . please contact administrator');
        $this->session->set_flashdata('message', $array);
      } else if($respon->status =='1'){
        $array=array('status' => '1','message' => 'Data has been Added.. ');
        $this->session->set_flashdata('message', $array);
      } else {
        $array=array('status' => '0','message' => $respon->data);
        $this->session->set_flashdata('message', $array);
      }
      redirect('workhour');
    }



    function edit_hour(){
        $id_company = $this->session->userdata('id');
        $id_hour = $this->input->post('id_hours');
        $info = $this->input->post('edit_info');
        $start_time = $this->input->post('edit_start');
        $end_time = $this->input->post('edit_end');


  
  
        $body = array(
            "start" =>  $start_time,
            "end" =>  $end_time,
            "info" =>  $info,
        );
  

        
      
        var_dump($body);
        exit;
  
  
  
       
        $respon = json_decode($this->APIHris->Apiput($this->url_hris."/shift/$id_hour", $body));

      

      
  
        if($respon==null){
          $array=array('status' => '0','message' => 'API not response . please contact administrator');
          $this->session->set_flashdata('message', $array);
        } else if($respon->status =='1'){
          $array=array('status' => '1','message' => 'Data has been Edited.. ');
          $this->session->set_flashdata('message', $array);
        } else {
          $array=array('status' => '0','message' => $respon->data);
          $this->session->set_flashdata('message', $array);
        }
        redirect('workhour');
      }
     




   

} 
?>