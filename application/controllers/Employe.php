<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    class employe extends CI_Controller{

        function __construct(){

            parent::__construct();
            
            // $this->load->helper('url');
            $this->load->helper('form');
            $this->load->library('session');
            $this->load->model("user_model");
            $this->load->model("APIDocman");
            $this->load->model("APILogbook");
            $this->load->model("APISimpok");        
            $this->load->model("APIHris");        
            $this->url_hris = $this->config->item('url-hris');
            $this->load->model('Model');
            $this->session_key = $this->config->item('session-key');
            $this->key = $this->session->userdata('log');

    
        } 

        function index(){

            $id_company = $this->session->userdata('id');
            

            $employe = json_decode($this->APIHris->Apiget($this->url_hris."/employee/company/$id_company"))->data;
            $employe_id = json_decode($this->APIHris->Apiget($this->url_hris."/employee/2"))->data;

            // var_dump($employe);
            // var_dump($employe_id);
            // exit;


            $data["employe"] = $employe;
            $data["id_company"] = $id_company;
            $data["employe_id"] = $employe_id;


            $key = $this->session->userdata('log');
    
            if ($key =="loveyou"){
                $this->load->view('frame/a_header');
                $this->load->view('frame/b_nav');
                $this->load->view('page/employe', $data);
                $this->load->view('frame/d_footer');    
            }else{
                $this->load->view('page/login');
            }    
        }


        function editEmploye(){

        $id_employee = $this->input->get('iemp');
        $id_company = $this->session->userdata('id');

        $edit = json_decode($this->APIHris->Apiget($this->url_hris."/employee/$id_employee"))->data;


        $kabupaten = [];
        $provinsi = json_decode($this->APIHris->Apiget($this->url_hris."/regency/provinsi"))->data;
            if($edit[0]->prov != null || $edit[0]->prov != ""){
                $kabupaten = json_decode($this->APIHris->Apiget($this->url_hris."/regency/kabupaten/prov/".$edit[0]->prov->id))->data;
            }else{
                $kabupaten = json_decode($this->APIHris->Apiget($this->url_hris."/regency/kabupaten/prov/".$provinsi[0]->id))->data;
            }
            $kecamatan = [];
            if($edit[0]->kec != null || $edit[0]->kab != ""){
                $kecamatan = json_decode($this->APIHris->Apiget($this->url_hris."/regency/kecamatan/kab/".$edit[0]->kab->id))->data;
            }else{
                $kecamatan = json_decode($this->APIHris->Apiget($this->url_hris."/regency/kecamatan/kab/".$kabupaten[0]->id))->data;
            }
        $work_hour = json_decode($this->APIHris->Apiget($this->url_hris."/shift/company/$id_company"))->data;
        $divisi = json_decode($this->APIHris->Apiget($this->url_hris."/divisi/company/$id_company"))->data;
        $grade = json_decode($this->APIHris->Apiget($this->url_hris."/grade/level/$id_company"))->data;
        $level = json_decode($this->APIHris->Apiget($this->url_hris."/level/company/$id_company"))->data;


        $data["provinsi"] = $provinsi;
        $data["kabupaten"] = $kabupaten;
        $data["kecamatan"] = $kecamatan;
        $data["work_hour"] = $work_hour;
        $data["divisi"] = $divisi;
        $data["level"] = $level;
        $data["grade"] = $level;
        $data["edit"] = $edit;

        $key = $this->session->userdata('log');
    

        $this->load->view('frame/a_header');
        $this->load->view('frame/b_nav');
        $this->load->view('page/editemploye', $data);
        $this->load->view('frame/d_footer');    

        }


        function saveedit(){

            $id_company = $this->session->userdata('id');
            $id_employee = $this->input->post('id_employe');
            $first_name = $this->input->post('edit_firstName');
            $alamat = $this->input->post('edit_alamat');
            $last_name = $this->input->post('edit_last_name');
            $provinsi = $this->input->post('add_provinsi');
            $nik = $this->input->post('edit_nik');
            $kabupaten = $this->input->post('add_kabupaten');
            $email = $this->input->post('edit_mail');
            $npwp = $this->input->post('edit_npwp');
            $kecamatan = $this->input->post('add_kecamatan');
            $phone = $this->input->post('edit_phone');
            $nation = $this->input->post('edit_nation');
            $work_hour = $this->input->post('add_work_hour');
            $divisi = $this->input->post('add_divisi');
            $level = $this->input->post('add_level');
            $status = $this->input->post('add_status');
            $role = $this->input->post('edit_role');
            $gaji = $this->input->post('edit_gaji');

      

            $body = array(
                "first_name" => $first_name,
                "last_name" => $last_name,
                "npwp" => $npwp,
                "id_divisi" => $divisi,
                "id_grade" => '',
                "id_job_level" => $level,
                "id_hour" => $work_hour,
                "id_struktur" =>'' ,
                "nik" => $nik,
                "address" => $alamat,
                "id_prov" => $provinsi,
                "id_kab" => $kabupaten,
                "id_kec" => $kecamatan,
                "national" => $nation,
                "phone" => $phone,
                "email" => $email,
                "status" => $status,
                "role" => $role,
                "gaji" => $id_company
              );

             

         

            $edit = json_decode($this->APIDocman->Apiput($this->url_hris."/employee/$id_employee", $body));

       
       
            if($edit==null){
                $array=array('status' => '0','message' => 'API not response . please contact administrator');
                $this->session->set_flashdata('message', $array);
              } else if($edit->status =='1'){
                $array=array('status' => '1','message' => 'Data has been Edited.. ');
                $this->session->set_flashdata('message', $array);
              } else {
                $array=array('status' => '0','message' => $edit->data);
                $this->session->set_flashdata('message', $array);
              }
              redirect('employe');
            }
        
      
}
