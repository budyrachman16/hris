<?php

class Login extends CI_Controller {
   
    function __construct(){
        parent::__construct();

        $this->load->helper('url');
		$this->load->helper('form');
        $this->load->library('session');
        $this->load->model("User_model");
        $this->load->model("APIHris");
        $this->url = $this->config->item('url-hris');
        $this->session_key = $this->config->item('session-key');
        $this->key = $this->session->userdata('log');

    }

    function index(){

        if($this->session->userdata("log") != $this->session_key){
            $this->load->view('page/login');
        } else {
            redirect("home");
        }
    }
    
    function user_login() {
        $username = $this->input->post('email');
        $password = $this->input->post('password');

     

        $username =  $username;
        // $password = md5($password);

        // var_dump($username);
        // var_dump($password);
        // exit;

        $body = array(
            "username" =>  $username,
            "password" =>  $password,
        );
        

        

        $respons = json_decode($this->APIHris->getLogin($this->url."/auth/verify", $body));
    


    
        if ($respons->status == '1' ) {
                // $respons = $respons->data[0];

               $newdata = array(
                    'log'  => $this->session_key,
                    'id'=> $respons->data[0]->id,
                    'name'=> $respons->data[0]->name,
                    'id_address'=> $respons->data[0]->id_address,
                    'active'=> $respons->data[0]->active,
                    'created_date'=> $respons->data[0]->created_date,
                    'alamat'=> $respons->data[0]->alamat,                    
                    'role'   => $respons->role,
                    'token' => $respons->token,
                );
                $this->session->set_userdata($newdata);
        } else {
                if($respon == null){
                    $array=array('status' => '0','message' => 'Check your username or password again');
                    $this->session->set_flashdata('message', $array);

                } else {
                    $array=array('status' => '0','message' => 'Check your username or password again');
                    $this->session->set_flashdata('message', $array);
                }
        }
        
        redirect('home');
    }


    
    function setSession($username) {
        $data = $this->set_userdata($newdata);
        $this->user_model->login_out($data->id_user,"1");
        $profile = $this->customer_model->getByIdUser($data->id_user);
        $sess_array = array(
            'log' => $this->session_key,
            'id_user' => "$data->id_user",
            'id_cust' => "$profile->id_cust",
            'username' => "$data->username",
            'role' => "$data->id_role",
            'nama' => "$profile->nm_depan"." "."$profile->nm_belakang",
            'email' => "$profile->email",
            'pekerjaan' => "$profile->pekerjaan",
            'url' => "$profile->url_profile"              
        );
        $this->session->set_userdata($sess_array);
    }

    function user_logout() { 

        $this->session->sess_destroy(); 
        redirect('login', 'refresh'); 
    }

    // function cekAdmin ( $nik ){

    //     $curl = curl_init();

    //     curl_setopt_array($curl, array(
    //     CURLOPT_URL => "localhost:8000/access/user/nik/$nik",
    //     CURLOPT_RETURNTRANSFER => true,
    //     CURLOPT_ENCODING => "",
    //     CURLOPT_MAXREDIRS => 10,
    //     CURLOPT_TIMEOUT => 0,
    //     CURLOPT_FOLLOWLOCATION => true,
    //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //     CURLOPT_CUSTOMREQUEST => "GET",
    //     CURLOPT_HTTPHEADER => array(
    //         "appsession: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0"
    //     ),
    //     ));

    //     $respons = curl_exec($curl);
    //     $respons = json_decode($respons);
    //     curl_close($curl);

    //     // if ($response->rst == '1' ) {
    //     //     $data = array(
    //     //         "id": $response,
    //     //         "status": "Admin",
    //     //         "nik": "D20003",
    //     //         "id_access": 1
   
    //     //    );
    //     // }

    //     var_dump($respons->data);
    //     exit;
    // }

}
?>
