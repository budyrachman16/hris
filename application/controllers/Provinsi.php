<?php

class provinsi extends CI_Controller{

    function __construct(){

        parent::__construct();
        
        // $this->load->helper('url');
	    $this->load->helper('form');
        $this->load->library('session');
        $this->load->model("user_model");
        $this->load->model("APIDocman");
        $this->load->model("APILogbook");
        $this->load->model("APISimpok");        
        $this->url = $this->config->item('url');
        $this->url_hris = $this->config->item('url-hris');
        $this->load->model('Model');
        $this->session_key = $this->config->item('session-key');

    }

    function index(){

          
      $provinsi = json_decode($this->APIDocman->ApigetTipe($this->url_hris."regency/provinsi"))->data;

      $data["provisinsi"] = $provisinsi;



        $key = $this->session->userdata('log');

        if ($key =="loveyou"){
            $this->load->view('frame/a_header');
            $this->load->view('frame/b_nav');
            $this->load->view('page/provinsi',$data);
            $this->load->view('frame/d_footer');    
        }else{
            $this->load->view('page/login');
        }
    }

    function inputType(){
        $file_type = $this->input->post('type_file');
        $file_ext = $this->input->post('file_ext');

        $arr = array(
            "file_type" =>  $file_type,
            "file_ext" => $file_ext,
        );

    $respon = json_decode($this->APIDocman->ApiPostTipe($this->url."/file/tipe/post", $arr));

     if($respon->status =='1'){
        $array=array('status' => '1','message' => 'Data has been added.. ');
        $this->session->set_flashdata('message', $array);
     }
      redirect('configfile');
    }


    function deleteType($id_type){
        $respon = json_decode($this->APIDocman->ApidelType($this->url."/file/tipe/delete/$id_type"));

        if($respon==null){
          $array=array('status' => '0','message' => 'API not response . please contact administrator');
          $this->session->set_flashdata('message', $array);
        } else if($respon->status =='1'){
          $array=array('status' => '0','message' => 'Data has been Deleted.. ');
          $this->session->set_flashdata('message', $array);
        } else {
          $array=array('status' => '0','message' => $respon->data);
          $this->session->set_flashdata('message', $array);
        }
        redirect('configfile');
      }



      function edit_type(){
        $id_type = $this->input->post('id_type');
        $file_type = $this->input->post('type_file_e');
        $file_ext = $this->input->post('file_ext_e');

        $body = array(
            "id_type" =>  $id_type,
            "file_type" =>  $file_type,
            "file_ext" => $file_ext,
        );
       
        $respon = json_decode($this->APIDocman->Apiput($this->url."/file/tipe/update/$id_type", $body));


        if($respon==null){
          $array=array('status' => '0','message' => 'API not response . please contact administrator');
          $this->session->set_flashdata('message', $array);
        } else if($respon->status =='1'){
          $array=array('status' => '1','message' => 'Data has been Edited.. ');
          $this->session->set_flashdata('message', $array);
        } else {
          $array=array('status' => '0','message' => $respon->data);
          $this->session->set_flashdata('message', $array);
        }
        redirect('configfile');
      }


    function postPartner(){

        $partner = $this->input->post('add_partner');
          $body = array(
            "partner" =>  $partner
        );

        $respon = json_decode($this->APIDocman->postPartner($this->url."/partner/post", $body));

       

        if($respon==null){
          $array=array('status' => '0','message' => 'API not response . please contact administrator');
          $this->session->set_flashdata('message', $array);
        } else if($respon->status =='1'){
          $array=array('status' => '1','message' => 'Data has been Added.. ');
          $this->session->set_flashdata('message', $array);
        } else {
          $array=array('status' => '0','message' => $respon->data);
          $this->session->set_flashdata('message', $array);
        }
        redirect('configfile');
    }


    function postPartnerType(){
      $type = $this->input->post('add_partner_type');
        $body = array(
          "type" =>  $type
      );

      $respon = json_decode($this->APIDocman->postPartnerType($this->url."/partner/tipe/post", $body));
      
      if($respon==null){
        $array=array('status' => '0','message' => 'API not response . please contact administrator');
        $this->session->set_flashdata('message', $array);
      } else if($respon->status =='1'){
        $array=array('status' => '1','message' => 'Data has been Added.. ');
        $this->session->set_flashdata('message', $array);
      } else {
        $array=array('status' => '0','message' => $respon->data);
        $this->session->set_flashdata('message', $array);
      }
      redirect('configfile');
  }

    function deletePartner($id){
      $respon = json_decode($this->APIDocman->ApiDelPartner($this->url."/partner/delete/$id"));

      if($respon==null){
        $array=array('status' => '0','message' => 'API not response . please contact administrator');
        $this->session->set_flashdata('message', $array);
      } else if($respon->status =='1'){
        $array=array('status' => '0','message' => 'Data has been Deleted.. ');
        $this->session->set_flashdata('message', $array);
      } else {
        $array=array('status' => '0','message' => $respon->data);
        $this->session->set_flashdata('message', $array);
      }
      redirect('configfile');
    }


    function deletePartnerType($id){
      $respon = json_decode($this->APIDocman->ApiDelPartnerType($this->url."/partner/tipe/delete/$id"));

      if($respon==null){
        $array=array('status' => '0','message' => 'API not response . please contact administrator');
        $this->session->set_flashdata('message', $array);
      } else if($respon->status =='1'){
        $array=array('status' => '0','message' => 'Data has been Deleted.. ');
        $this->session->set_flashdata('message', $array);
      } else {
        $array=array('status' => '0','message' => $respon->data);
        $this->session->set_flashdata('message', $array);
      }
      redirect('configfile');
    }

} 
?>