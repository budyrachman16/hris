<div class="page">

<!-- Header Page -->


  <!-- Header Page -->

  <!-- Content Page -->
  <div class="page-content">
    <h1 class="page-title">Work Hour List</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url('home')?>">Home</a></li>
        <li class="breadcrumb-item active">Work Hour</li>
      </ol>
    <!-- Body Page -->
    <div class="panel">
      <header class="panel-heading">
      <h3 class="panel-title">
      <br>
        <button id="add" class="btn btn-primary" onClick="modal_create()"  type="button" data-target="#modal_create"
          data-toggle="modal">
            <i class="icon md-alarm-check" aria-hidden="true"></i> Add Work Hour
        </button>
      </header>
      <div class="panel-body collapse show" id="exampleFooCollapsePanel">
        <table class="table table-bordered table-hover toggle-circle" id="exampleFooCollapse"
          data-paging="true" data-filtering="true" data-sorting="true">
          <thead>
            <tr>
              <th data-name="id" data-type="number" data-breakpoints="xs" width=50px>ID</th>
              <th data-name="firstName" width=200px>Start Time</th>
              <th data-name="lastName" width=200px>End Time</th>
              <th data-name="nik" width=200px>Info</th>
              <th data-name="divisi" width=200px>Action</th>
              <!-- <th data-name="jobTitle" data-breakpoints="all" >Level</th>
              <th data-name="email" data-breakpoints="all" >Email</th>
              <th data-name="email" data-breakpoints="all" >Telephone</th>
              <th data-name="jobTitle" data-breakpoints="all" >Alamat</th>              
              <th data-name="jobTitle" data-breakpoints="all" >Action</th>               -->
            </tr>
          </thead>   
          <tbody> 
            <?php $i = 1; foreach ($work_hour as $wh): ?>
                <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?= $wh->start ?></td>
                    <td><?= $wh->end ?></td>
                    <td><?= $wh->info ?></td>
                
                    <td class="actions">
                      <!-- Edit FIle -->
                      <a href="#" onClick="return edit_hour('<?php echo $wh->id_hour; ?>','<?php echo $wh->start; ?>','<?php echo $wh->end;?>','<?php echo $wh->info;?>')""  class="btn btn-info btn-xs" data-toggle="modal" data-target="#modal_edit"><i class="icon md-edit"></i> Edit </a>
                      <!-- Delete -->
                      <!-- <a href="#" onclick='return del_confirm()' class="btn btn-danger btn-xs"><i class="icon md-delete"></i> Delete </a>  -->
                      <!-- <detail> -->
                      <!-- <a href="#" onclick='return del_confirm()' class="btn btn-success btn-xs"><i class="icon md-receipt"></i> Detail </a>  -->
                    </td>    
                </tr>                      
            <?php endforeach; ?>
          </tbody>           
        </table>
      </div>
    </div>
  </div>


  <div class="modal fade" id="modal_create" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <h3 class="modal-title" id="myModalLabel">Add Division</h3>
          </div>
        <!-- <div class="modal-header"></div> -->
        <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('workhour/post_workhour')?>" onsubmit="disableButton()">
          <div class="modal-body">             
              <div class="form-group">
                  <label class="control-label col-xs-3">Info</label>
                  <div class="col-xs-8">
                      <input name="add_info" id='add_info' class="form-control" type="text" >
                  </div>
              </div>
              <div class="form-group">
                  <label class="control-label col-xs-3">Start Time</label>
                  <div class="col-xs-8">
                      <input name="add_star_time" id='add_star_time' class="form-control" type="time" >
                  </div>
              </div>
              <div class="form-group">
                  <label class="control-label col-xs-3">End Time</label>
                  <div class="col-xs-8">
                      <input name="add_end_time" id='add_end_time' class="form-control" type="time" >
                  </div>
              </div>
              <div class="modal-footer">
                  <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                  <button class="btn btn-info" type="submit">Input</button>
              </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <h3 class="modal-title" id="myModalLabel">Edit Hour</h3>
              </div>
                <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('workhour/edit_hour')?>" onsubmit="disableButton()">
                    <div class="modal-body">
                        <div class="form-group">
                                <input name="id_hours" id='id_hours' class="form-control" type="text" hidden>
                                  <div class="form-group">
                                    <label class="control-label col-xs-3">Info</label>
                                      <div class="col-xs-8">
                                          <input name="edit_info" id='edit_info' class="form-control" type="text" >
                                      </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-xs-3">Start Time</label>
                                      <div class="col-xs-8">
                                          <input name="edit_start" id='edit_start' class="form-control" type="text" >
                                      </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-xs-3">End Time</label>
                                      <div class="col-xs-8">
                                          <input name="edit_end" id='edit_end' class="form-control" type="text" >
                                      </div>
                                  </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                                <button class="btn btn-info" type="submit">Edit</button>
                            </div>
                        </div>
                    </div>
                </form>
          </div>
      </div>
  </div>


</div>



























<script type="text/javascript">



  $(function () {
   $('#datetimepicker1').datetimepicker({
      format: 'hh:mm:ss'
   });
  });

  
  function upload(){

    let upload_file = document.getElementById("upload_file").value;
    let nama_c = document.getElementById("nama_c").value;
    let nama_file = document.getElementById("nama_file").value;
    let input_file = document.getElementById("input_file").value;
    let file_type = document.getElementById("file_type").value;
    

    project = $.ajax({
            data : { id_project : id_project },
            type:"POST",
            url: "<?php echo site_url('dokumen/getProject');?>",
            async: false
      }).responseText;
   
  }


  function edit_hour(id_hour,start,end,info){
    document.getElementById("id_hours").value = id_hour
    document.getElementById("edit_start").value = start;
    document.getElementById("edit_end").value = end;
    document.getElementById("edit_info").value = info;
  }


  function disableButton() {
    var btn = document.getElementById('btn_add_submit');
    btn.disabled = true;
    btn.innerText = 'Posting...';
}
</script>
<!--END MODAL EDIT-->
  
    
    
