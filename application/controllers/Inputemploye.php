<?php

class inputemploye extends CI_Controller{

    function __construct(){

        parent::__construct();
        
        // $this->load->helper('url');
	    $this->load->helper('form');
        $this->load->library('session');
        $this->load->model("user_model");
        $this->load->model("APIDocman");
        $this->load->model("APILogbook");
        $this->load->model("APISimpok");     
        $this->load->model("APIHris");        
        $this->url_hris = $this->config->item('url-hris');
        $this->url = $this->config->item('url');
        $this->load->model('Model');
        $this->session_key = $this->config->item('session-key');

    }

    function index(){


      $id_company = $this->session->userdata('id');
            

      $provinsi = json_decode($this->APIHris->Apiget($this->url_hris."/regency/provinsi"))->data;
      $work_hour = json_decode($this->APIHris->Apiget($this->url_hris."/shift/company/$id_company"))->data;
      $divisi = json_decode($this->APIHris->Apiget($this->url_hris."/divisi/company/$id_company"))->data;
      $level = json_decode($this->APIHris->Apiget($this->url_hris."/grade/level/$id_company"))->data;
      // $kabupaten = $kabupaten;
      // $kabupaten = json_decode($this->APIHris->Apiget($this->url_hris."/kecamatan/kab/3173"))->data;
      // $kabupaten = json_decode($this->APIHris->Apiget($this->url_hris."/kecamatan/kab/3173"))->data;




      $data["provinsi"] = $provinsi;
      $data["id_company"] = $id_company;
      $data["work_hour"] = $work_hour;
      $data["divisi"] = $divisi;
      $data["level"] = $level;
      // $data["kabupaten"] = $kabupaten; 



            // var_dump($data);
        $key = $this->session->userdata('log');

        if ($key =="loveyou"){
            $this->load->view('frame/a_header');
            $this->load->view('frame/b_nav');
            $this->load->view('page/inputemploye',$data);
            $this->load->view('frame/d_footer');    
        }else{
            $this->load->view('page/login');
        }
    }



    function getEmployeId($id){


      $id_company = $this->session->userdata('id');
            

      $provinsi = json_decode($this->APIHris->Apiget($this->url_hris."/regency/provinsi"))->data;
      $work_hour = json_decode($this->APIHris->Apiget($this->url_hris."/shift/company/$id_company"))->data;
      $divisi = json_decode($this->APIHris->Apiget($this->url_hris."/divisi/company/$id_company"))->data;
      $level = json_decode($this->APIHris->Apiget($this->url_hris."/grade/level/$id_company"))->data;
      // $kabupaten = $kabupaten;
      // $kabupaten = json_decode($this->APIHris->Apiget($this->url_hris."/kecamatan/kab/3173"))->data;
      // $kabupaten = json_decode($this->APIHris->Apiget($this->url_hris."/kecamatan/kab/3173"))->data;




      $data["provinsi"] = $provinsi;
      $data["id_company"] = $id_company;
      $data["work_hour"] = $work_hour;
      $data["divisi"] = $divisi;
      $data["level"] = $level;
      // $data["kabupaten"] = $kabupaten; 



            // var_dump($data);
        $key = $this->session->userdata('log');

        if ($key =="loveyou"){
            $this->load->view('frame/a_header');
            $this->load->view('frame/b_nav');
            $this->load->view('page/inputemploye',$data);
            $this->load->view('frame/d_footer');    
        }else{
            $this->load->view('page/login');
        }
    }



    function getKabupaten(){

      $id =  $_POST['id'];
      

      // var_dump($id);
      // exit;


      $kabupaten = json_decode($this->APIHris->Apiget($this->url_hris."/regency/kabupaten/prov/$id"))->data;

      echo json_encode($kabupaten);
     

    }


    function getKecamatan(){

      $id =  $_POST['id'];
      


      $kecamatan = json_decode($this->APIHris->Apiget($this->url_hris."/regency/kecamatan/kab/$id"))->data;

      echo json_encode($kecamatan);


    }
     

    


    function postEmploye(){

      $id_company = $this->session->userdata('id');
      

      
      $first_name = $this->input->post('add_first_name');
      $alamat = $this->input->post('add_alamat');
      $last_name = $this->input->post('add_last_name');
      $provinsi = $this->input->post('add_provinsi');
      $nik = $this->input->post('add_nik');
      $kabupaten = $this->input->post('add_kabupaten');
      $npwp = $this->input->post('add_npwp');
      $kecamatan = $this->input->post('add_kecamatan');
      $phone = $this->input->post('add_phone');
      $nation = $this->input->post('add_nation');
      $email = $this->input->post('add_email');

      $work_hour = $this->input->post('add_work_hour');
      $divisi = $this->input->post('add_divisi');
      $level = $this->input->post('add_level');
      $gaji = $this->input->post('add_gaji');
      $status = $this->input->post('add_status');
      $role = $this->input->post('add_role');
      $grade = $this->input->post('add_grade');
      $struktur = $this->input->post('add_struktur');


      if($grade == NULL){
          $grade = "";
      }

      if ($struktur == NULL){
          $struktur = "";
      }

      
      $body = array(
        "first_name" => $first_name,
        "last_name" => $alamat,
        "npwp" => $npwp,
        "id_company" => $id_company,
        "id_hour" => $work_hour,
        "id_divisi"=>  $divisi,
        "id_grade" => $grade,
        "id_job_level"=>  $level,
        "id_struktur" => $struktur,
        "nik" => $nik,
        "gaji" => $gaji,
        "address"=>  $alamat,
        "id_prov"=>  $provinsi,
        "id_kab" => $kabupaten,
        "id_kec" => $kecamatan,
        "national"=> $nation,
        "phone" => $phone,
        "email"=>  $email,
        "status" => $email,
        "role" => $role
    );

    // var_dump($body);
    // exit;

    $respon = json_decode($this->APIHris->ApiPost($this->url_hris."/employee", $body));

    // var_dump($respon);
    // exit;

      if($respon==null){
        $array=array('status' => '0','message' => 'API not response . please contact administrator');
        $this->session->set_flashdata('message', $array);
      } else if($respon->status =='1'){
        $array=array('status' => '1','message' => 'Data has been Added.. ');
        $this->session->set_flashdata('message', $array);
      } else {
        $array=array('status' => '0','message' => $respon->data);
        $this->session->set_flashdata('message', $array);
      }
      redirect('employe');
  }

    

    function deletePartnerType($id){
      $respon = json_decode($this->APIDocman->ApiDelPartnerType($this->url."/partner/tipe/delete/$id"));

      // var_dump($respon);
      // exit;
      if($respon==null){
        $array=array('status' => '0','message' => 'API not response . please contact administrator');
        $this->session->set_flashdata('message', $array);
      } else if($respon->status =='1'){
        $array=array('status' => '0','message' => 'Data has been Deleted.. ');
        $this->session->set_flashdata('message', $array);
      } else {
        $array=array('status' => '0','message' => $respon->data);
        $this->session->set_flashdata('message', $array);
      }
      redirect('partnertype');
    }


    function edit_partner_type(){
      $id = $this->input->post('id');
      $type = $this->input->post('type');
 

      $body = array(
          "type" =>  $type,
      );
     
   

      $respon = json_decode($this->APIDocman->ApiputPartner($this->url."/partner/tipe/update/$id", $body));

      // var_dump($respon);
      // exit;

      if($respon==null){
        $array=array('status' => '0','message' => 'API not response . please contact administrator');
        $this->session->set_flashdata('message', $array);
      } else if($respon->status =='1'){
        $array=array('status' => '1','message' => 'Data has been Edited.. ');
        $this->session->set_flashdata('message', $array);
      } else {
        $array=array('status' => '0','message' => $respon->data);
        $this->session->set_flashdata('message', $array);
      }
      redirect('partnertype');
    }


} 
?>