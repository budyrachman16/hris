<?php

class Dokumen extends CI_Controller{

    function __construct(){

        parent::__construct();
        
        // $this->load->helper('url');
	    $this->load->helper('form');
        $this->load->library('session');
        $this->load->model("user_model");
        $this->load->model("APIDocman");
        $this->load->model("APILogbook");
        $this->load->model("APISimpok");        
        $this->url = $this->config->item('url');
        $this->load->model('Model');
        $this->session_key = $this->config->item('session-key');
        $this->key = $this->session->userdata('log');

        if ($this->key !=  $this->session_key){
            $array=array('status' => '0','message' => 'Session Expired');
            $this->session->set_flashdata('message', $array);
            redirect("login");
        }

    }

    function index() {
        
        $this->load->view('frame/a_header');
        $this->load->view('frame/b_nav');
        $this->load->view('page/dokumen');
        $this->load->view('frame/d_footer');         
    }


     function uploadFile(){ 

        date_default_timezone_set('Asia/Jakarta');

        $partner = $this->input->post('add_partner');
        $partner_type = $this->input->post('add_partner_type');
        $doc_type = $this->input->post('doc_type');
        $id_user = implode("|", $this->input->post('add_division'));
        $project = $this->input->post('add_project');
        $tmpFile = $_FILES['userfile']['tmp_name'];
        $typeFile = $_FILES['userfile']['type'];
        $nameFile = $_FILES['userfile']['name'];
        $start_date = $this->input->post('add_date_start');
        $end_date = $this->input->post('add_date_end');
        $uploaded_by = $this->session->userdata('id_user');
        $date = date("y-m-d");

        $start_date = date('Y-m-d', strtotime($start_date));
        $end_date = date('Y-m-d', strtotime($end_date));



        if ( $tmpFile == "" && NULL ) {
            $array=array('status' => '0','message' => 'File Kosong');          
        } else if ($typeFile == "" && NULL) {
            $array=array('status' => '0','message' => 'Type File Salah (masukan type file .JPG, .JPEG, .PDF');    
        }else{      
            $respon = $this->APIDocman->uploadFile($project, 'doc', $tmpFile, $typeFile, $nameFile);

            
            if($respon->status=='0'){
                $array=array('status' => '0','message' => 'Error File Type');
                $this->session->set_flashdata('message', $array);
                redirect('dokumen');
                } else if($respon->status =='1'){
                $array=array('status' => '1','message' => $respon->message);
                $this->session->set_flashdata('message', $array);
            };


            $file_name = $respon->data->file_name;
            $file_true_name = $respon->data->file_true_name;
            $type = $respon->data->type;
            $folder_name = $respon->data->folder_name;


            if ( $respon->status == '0' ) {
                redirect ('dokumen');
            } else {
                $arr2 = array(
                    'id_project' => $project,
                    'file_name' => $file_name,
                    'file_true_name' => $file_true_name,
                    'path_folder' => $folder_name,
                    'file_type' => $type,
                    'id_user' => $id_user,
                    'file_date'=>$date,
                    'partner'=> $partner,
                    'partner_type'=> $partner_type,
                    'start_date'=>$start_date,
                    'doc_type'=>$doc_type,
                    'end_date'=>$end_date,
                    'upload_by'=>$uploaded_by

                );
                $arr2 = json_encode($arr2);

                // var_dump($arr2);
                // exit;

                $curl = curl_init();
                curl_setopt_array($curl, array(
                CURLOPT_URL => "http://aplikasi2.gratika.id/docman_coreapi/doc/post",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $arr2,
                CURLOPT_HTTPHEADER => array(
                    "appsession: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg3MzY5ODY2LCJleHAiOjE2MTg5MDU4NjZ9.NRopsAqEeLkjwuSr9R9dHE_W8dKT551pjIvEqhFYnP0",
                    "Content-Type: application/json"
                ),
                ));
                $response = curl_exec($curl);
                curl_close($curl);
                        // var_dump ($response);
                        // exit;
                if($response==null){
                    $array=array('status' => '0','message' => 'API not response . please contact administrator');
                    $this->session->set_flashdata('message', $array);
                  } else if($respon->status =='1'){
                    $array=array('status' => '1','message' => 'Data has been add.. ');
                    $this->session->set_flashdata('message', $array);
                  } else {
                    $array=array('status' => '0','message' => $respon->data);
                    $this->session->set_flashdata('message', $array);
                  }
                  redirect('dokumen');
                }   
        }
    }

    function postDokumen(){ 
        $project = $this->input->post('add_project');
        $tmpFile = $_FILES['userfile']['tmp_name'];
        $nameFile = $_FILES['userfile']['name'];
        $typeFile = $_FILES['userfile']['type'];
      

        $body = array(
                "id_project" =>$project,
                "id_user" =>$id_user,
                "file_name" =>$nameFile,
                "file_true_name" =>$tmpFile,
                "fie_type" => $typeFile,
            );

        $respon = json_decode($this->APIDocman->uploadFile($this->url."/doc/upload/file", $body));
    }


    function sendEmail(){

            $id_doc = $this->input->post('id_docman');
            $email = $this->input->post('email');
            $subject = $this->input->post('subject');
            $file_name = $this->input->post('file_name');
            $file_type = $this->input->post('file_type');
            $sender = $this->session->userdata('full_name');
            $sender_email = $this->session->userdata('email');
            $folder = $this->input->post('path_folder');
            $body = $this->input->post('body');
            $reciver = $this->input->post('reciver');

            $body = array(
            "id_doc" =>  $id_doc,
            "body" =>  $body,
            "subject" =>  $subject,
            "to" =>  $reciver,
            "to_email" =>  $email,
            "sender" =>  $sender,
            "sender_email" =>  $sender_email,
            "folder" =>  $folder,
            "file" =>  $file_name,
            "type" =>  $file_type,
        );

        // var_dump($body);
        // exit;


        $respons = json_decode($this->APIDocman->sendEmail($this->url."/mail/send", $body));

        

        if($respons==null){
            $array=array('status' => '0','message' => 'API not response . please contact administrator');
            $this->session->set_flashdata('message', $array);
        } else if($respons->status =='1'){
            $array=array('status' => '1','message' => 'Data has been Send.. ');
            $this->session->set_flashdata('message', $array);
        } else {
            $array=array('status' => '0','message' => $respons->data);
            $this->session->set_flashdata('message', $array);
        }
        redirect('dokumen');
    }




   function getEmailByD(){


    $html = "";
    $html .= '<table id="tblData" class="table table-bordered table-striped">';
    $html .= '<thead>';
    $html .= '<tr>';
    $html .= '<th>Destination</th>';
    $html .= '<th>Sender</th>';
    $html .= '<th>Send Date</th>';
    $html .= '</tr>';
    $html .= '</thead>';

    $id =  $_POST['id'];


    $curl = curl_init();
    curl_setopt_array($curl, array(
    CURLOPT_URL => "http://aplikasi2.gratika.id/docman_coreapi/mail/doc/$id",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "Postman-Token: 2cd10b46-c5fc-44bd-8b9d-3160d713d60e",
        "appsession: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg2ODc2ODE0LCJleHAiOjE2MTg0MTI4MTR9.SmHk15zqTOR9e-p45GxBSY87qw1tJJEp-47neT6J95Q",
        "cache-control: no-cache"
    ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
    $response = json_decode($response);


        $html .= '<tbody>';
    foreach ($response->data as $value) {

    
        $html .= '<tr>';
        $html .= '<td>'.$value->to_email.'</td>';
        $html .= '<td>'.$value->sender.'</td>';
        $html .= '<td>'.$value->send_date.'</td>';
        $html .= '</tr>';
       
    }
        $html .= '</tbody>';
        $html .= '</table>';


   
  



    echo $html;




   
    // echo json_encode($response);
    }



    function revisiDoc(){

        date_default_timezone_set('Asia/Jakarta');
        $id_doc = $this->input->post('id_docman_r');
        $id_project = $this->input->post('id_project_r');
        $partner = $this->input->post('partner_r');
        $partner_type = $this->input->post('partner_typer');
        $doc_type = $this->input->post('doc_typer');
        $id_user = $this->input->post('id_user_r');;
        $tmpFileSupport = $_FILES['upload_supporting']['tmp_name'];//support
        $typeFileSupport = $_FILES['upload_supporting']['type'];//support
        $nameFileSupport = $_FILES['upload_supporting']['name'];//support
        $tmpFileNewVer = $_FILES['upload_new']['tmp_name'];//newversion
        $typeFileNewVer = $_FILES['upload_new']['type'];//newversion
        $nameFileNewVer = $_FILES['upload_new']['name'];//newversion
        $start_date = $this->input->post('start_date_r');
        $end_date = $this->input->post('end_date_r');
        $uploaded_by = $this->session->userdata('id_user');
        $date = date("y-m-d");

        if ($tmpFileSupport == "" && $tmpFileNewVer == "" ){
            $array=array('status' => '0','message' => 'File Kosong');
            $this->session->set_flashdata('message', $array);
            redirect('dokumen');
        } else if ($typeFileSupport == "" && $typeFileNewVer == ""){
            $array=array('status' => '0','message' => 'Type File Salah (masukan type file .JPG, .JPEG, .PDF');   
            $this->session->set_flashdata('message', $array);
            redirect('dokumen');
        }

        if ($tmpFileSupport != "" ){
            $responSupportFile = $this->APIDocman->uploadFile($id_project, 'support', $tmpFileSupport, $typeFileSupport, $nameFileSupport);


            if($responSupportFile->status=='0' || $responSupportFile == NULL) {
                $array=array('status' => '0','message' => 'Error File Type Or API Not Response');
                $this->session->set_flashdata('message', $array);
                redirect('dokumen');
                
            } else if($responSupportFile->status == '1') {

                $file_name = $responSupportFile->data->file_name;
                $file_true_name = $responSupportFile->data->file_true_name;
                $type = $responSupportFile->data->type;
                $folder_name = $responSupportFile->data->folder_name;
    
                $arr2 = array(
                    'id_project' => $id_project,
                    'file_name' => $file_name,
                    'file_true_name' => $file_true_name,
                    'path_folder' => $folder_name,
                    'file_type' => $type,
                    'id_user' => $id_user,
                    'file_date'=>$date,
                    'partner'=> $partner,
                    'partner_type'=> $partner_type,
                    'start_date'=>$start_date,
                    'end_date'=>$end_date,
                    'doc_type'=>$doc_type,
                    'upload_by'=>$uploaded_by
                );



                $responSupport = $this->APIDocman->ApiUploadSupport($this->url."/doc/post/support", $arr2);
                $responSupport = Json_decode($responSupport);


                $link = array(
                    'id_doc'=> $id_doc,
                    'id_link'=> $responSupport->id_docman
                );


                // var_dump($link);
                // exit;

                $link = json_encode($link);
                $curl = curl_init();

                curl_setopt_array($curl, array(
                  CURLOPT_URL => $this->url."/link/post",
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 0,
                  CURLOPT_FOLLOWLOCATION => true,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => "POST",
                  CURLOPT_POSTFIELDS => $link,
                  CURLOPT_HTTPHEADER => array(
                    "appsession: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJhZG1pbiIsInVzcm4iOiJhZG1pbiIsInB3ZCI6IjVlYmUyMjk0ZWNkMGUwZjA4ZWFiNzY5MGQyYTZlZTY5IiwiaWF0IjoxNTg2ODc2ODE0LCJleHAiOjE2MTg0MTI4MTR9.SmHk15zqTOR9e-p45GxBSY87qw1tJJEp-47neT6J95Q",
                    "Content-Type: application/json"
                  ),
                ));
                
                $response = curl_exec($curl);

                if($responSupport==null){
                    $array=array('status' => '0','message' => 'API not response . please contact administrator');
                    $this->session->set_flashdata('message', $array);
                  } else if($responSupport->status =='1'){
                    $array=array('status' => '1','message' => 'Data has been add.. ');
                    $this->session->set_flashdata('message', $array);
                  } else {
                    $array=array('status' => '0','message' => $responSupport->data);
                    $this->session->set_flashdata('message', $array);
                  }
                  redirect('dokumen');
                }   
        };

        if ($tmpFileNewVer != "" ){
            $responNewVerFile = $this->APIDocman->uploadFile($id_project, 'doc', $tmpFileNewVer, $typeFileNewVer, $nameFileNewVer);  

            if($responNewVerFile->status=='0' || $responNewVerFile == NULL) {
                $array=array('status' => '0','message' => 'Error File Type Or API Not Response');
                $this->session->set_flashdata('message', $array);
                redirect('dokumen');
                
            } else if($responNewVerFile->status == '1') {

                $file_name = $responNewVerFile->data->file_name;
                $file_true_name = $responNewVerFile->data->file_true_name;
                $type = $responNewVerFile->data->type;
                $folder_name = $responNewVerFile->data->folder_name;
    
                $arr3 = array(
                    'id_project' => $id_project,
                    'file_name' => $file_name,
                    'file_true_name' => $file_true_name,
                    'path_folder' => $folder_name,
                    'file_type' => $type,
                    'id_user' => $id_user,
                    'file_date'=>$date,
                    'partner'=> $partner,
                    'partner_type'=> $partner_type,
                    'start_date'=>$start_date,
                    'end_date'=>$end_date,
                    'doc_type'=>$doc_type,
                    'upload_by'=>$uploaded_by
                );


                $responNewVer = $this->APIDocman->ApiUploadNewVer($this->url."/doc/post", $arr3);
                $responNewVer = Json_decode($responNewVer);

            
                if($responNewVer == Null){
                    $array=array('status' => '0','message' => 'API not response . please contact administrator');
                    $this->session->set_flashdata('message', $array);
                  } else if($responNewVer->status =='1'){
                    $array=array('status' => '1','message' => 'Data has been add.. ');
                    $this->session->set_flashdata('message', $array);
                  } else {
                    $array=array('status' => '0','message' => $responNewVer->message);
                    $this->session->set_flashdata('message', $array);
                  }
                  redirect('dokumen');
                }   
               

        };
            
    } 
} 
?>
