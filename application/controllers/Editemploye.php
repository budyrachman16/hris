<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    class editemploye extends CI_Controller{

        function __construct(){

            parent::__construct();
            
            // $this->load->helper('url');
            $this->load->helper('form');
            $this->load->library('session');
            $this->load->model("user_model");
            $this->load->model("APIDocman");
            $this->load->model("APILogbook");
            $this->load->model("APISimpok");        
            $this->load->model("APIHris");        
            $this->url_hris = $this->config->item('url-hris');
            $this->load->model('Model');
            $this->session_key = $this->config->item('session-key');
            $this->key = $this->session->userdata('log');

    
        } 

        function index(){

            $id_company = $this->session->userdata('id');
            

            $employe = json_decode($this->APIHris->Apiget($this->url_hris."/employee/company/$id_company"))->data;
            $employe_id = json_decode($this->APIHris->Apiget($this->url_hris."/employee/$id"))->data;


            $data["employe"] = $employe;
            $data["id_company"] = $id_company;
            $data["employe_id"] = $employe_id;

            $key = $this->session->userdata('log');
    
            if ($key =="loveyou"){
                $this->load->view('frame/a_header');
                $this->load->view('frame/b_nav');
                $this->load->view('page/editemploye', $data);
                $this->load->view('frame/d_footer');    
            }else{
                $this->load->view('page/login');
            }    
        }


        
      
}