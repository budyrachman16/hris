<?php

class kecamatan extends CI_Controller{

    function __construct(){

        parent::__construct();
        
        // $this->load->helper('url');
	    $this->load->helper('form');
        $this->load->library('session');
        $this->load->model("user_model");
        $this->load->model("APIDocman");
        $this->load->model("APILogbook");
        $this->load->model("APISimpok");        
        $this->url = $this->config->item('url');
        $this->load->model('Model');
        $this->session_key = $this->config->item('session-key');

    }

    function index(){

    


            // var_dump($data);
        $key = $this->session->userdata('log');

        if ($key =="loveyou"){
            $this->load->view('frame/a_header');
            $this->load->view('frame/b_nav');
            $this->load->view('page/kecamatan');
            $this->load->view('frame/d_footer');    
        }else{
            $this->load->view('page/login');
        }
    }

    


    function postPartnerType(){

      $type = $this->input->post('add_partner_type');

        $body = array(
          "type" =>  $type
      );

      $respon = json_decode($this->APIDocman->postPartnerType($this->url."/partner/tipe/post", $body));

    //   var_dump($respon);
    //   exit;

      if($respon==null){
        $array=array('status' => '0','message' => 'API not response . please contact administrator');
        $this->session->set_flashdata('message', $array);
      } else if($respon->status =='1'){
        $array=array('status' => '1','message' => 'Data has been Added.. ');
        $this->session->set_flashdata('message', $array);
      } else {
        $array=array('status' => '0','message' => $respon->data);
        $this->session->set_flashdata('message', $array);
      }
      redirect('partnertype');
  }

    

    function deletePartnerType($id){
      $respon = json_decode($this->APIDocman->ApiDelPartnerType($this->url."/partner/tipe/delete/$id"));

      // var_dump($respon);
      // exit;
      if($respon==null){
        $array=array('status' => '0','message' => 'API not response . please contact administrator');
        $this->session->set_flashdata('message', $array);
      } else if($respon->status =='1'){
        $array=array('status' => '0','message' => 'Data has been Deleted.. ');
        $this->session->set_flashdata('message', $array);
      } else {
        $array=array('status' => '0','message' => $respon->data);
        $this->session->set_flashdata('message', $array);
      }
      redirect('partnertype');
    }


    function edit_partner_type(){
      $id = $this->input->post('id');
      $type = $this->input->post('type');
 

      $body = array(
          "type" =>  $type,
      );
     
   

      $respon = json_decode($this->APIDocman->ApiputPartner($this->url."/partner/tipe/update/$id", $body));

      // var_dump($respon);
      // exit;

      if($respon==null){
        $array=array('status' => '0','message' => 'API not response . please contact administrator');
        $this->session->set_flashdata('message', $array);
      } else if($respon->status =='1'){
        $array=array('status' => '1','message' => 'Data has been Edited.. ');
        $this->session->set_flashdata('message', $array);
      } else {
        $array=array('status' => '0','message' => $respon->data);
        $this->session->set_flashdata('message', $array);
      }
      redirect('partnertype');
    }


} 
?>