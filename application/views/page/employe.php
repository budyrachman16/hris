  <div class="page">
  
  <!-- Header Page -->
    <!-- Header Page -->

    <!-- Content Page -->
    <div class="page-content">
      <h1 class="page-title">Employe List</h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo site_url('home')?>">Home</a></li>
          <li class="breadcrumb-item active">Employe</li>
        </ol>
      <!-- Body Page -->
      <div class="panel">
        <header class="panel-heading">
           <h3 class="panel-title">
           <br>
            <a href="<?php echo site_url('inputemploye')?>">
              <button id="add" class="btn btn-primary" type="button">
                <i class="icon md-account-add" aria-hidden="true"></i> Add Employe
              </button>
            </a>
          </h3>
        </header>
        <div class="panel-body collapse show" id="exampleFooCollapsePanel">
          <table class="table table-bordered table-hover toggle-circle" id="exampleFooCollapse"
            data-paging="true" data-filtering="true" data-sorting="true">
            <thead>
              <tr>
                <th data-name="id" data-type="number" data-breakpoints="xs" width=50px>ID</th>
                <th data-name="firstName" width=200px>First Name</th>
                <th data-name="lastName" width=200px>Last Name</th>
                <th data-name="nik" width=200px>NIK</th>
                <th data-name="divisi" width=200px>Divisi</th>
                <th data-name="jobTitle" data-breakpoints="all" >Level</th>
                <th data-name="email" data-breakpoints="all" >Email</th>
                <th data-name="email" data-breakpoints="all" >Telephone</th>
                <th data-name="jobTitle" data-breakpoints="all" >Alamat</th>              
                <th data-name="jobTitle" data-breakpoints="all" >Action</th>              
              </tr>
            </thead>   
            <tbody> 
              <?php $i = 1; foreach ($employe as $e): ?>
                  <tr>
                      <td><?php echo $i++; ?></td>
                      <td><?= $e->first_name ?></td>
                      <td><?= $e->last_name ?></td>
                      <td><?= $e->nik ?></td>
                      <td><?php if ($e->divisi == Null || $e->divisi == ""){
                          echo ("Unknown");
                      } else {
                        echo $e->divisi->name;
                      }
                      ?></td>    
                      <td><?php if ($e->job_level == Null || $e->job_level == ""){
                          echo ("Unknown");
                      } else {
                        echo $e->job_level->name;
                      }
                      ?></td>     
                      <td><?= $e->email ?></td>
                      <td><?= $e->telephone ?></td>
                      <td><?= $e->alamat ?></td>              
                  
                      <td class="actions">
                        <!-- Edit FIle -->
                        <a href="<?php echo site_url("employe/editEmploye?iemp=".$e->id_employee); ?>" onClick="return edit_employe('<?php echo $e->id_employee; ?>',
                        '<?php echo $e->company; ?>',
                        '<?php echo $e->address; ?>',
                        '<?php echo $e->first_name ?>',
                        '<?php echo $e->last_name; ?>',
                        '<?php echo $e->npwp; ?>',
                        '<?php echo $e->nik; ?>')"  class="btn btn-info btn-xs"><i class="icon md-edit" ></i> Edit </a>
                        <!-- Delete -->
                        <!-- <a href="#" onclick='return del_confirm()' class="btn btn-danger btn-xs"><i class="icon md-delete"></i> Delete </a>  -->
                        <!-- <detail> -->
                        <!-- <a href="#" onclick='return del_confirm()' class="btn btn-success btn-xs" ><i class="icon md-receipt"></i> Detail </a>  -->
                      </td>    
                  </tr>                      
              <?php endforeach; ?>
            </tbody>            
          </table>
        </div>
      </div>

      <div class="modal fade" id="modal_detail" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <h3 class="modal-title" id="myModalLabel">Detail Employe</h3>
                  </div>
                    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('division/editDivision')?>" onsubmit="disableButton()">
                        <div class="modal-body">
                            <div class="form-group">
                                    <input name="id_division" id='id_division' class="form-control" type="text" hidden >
                                <label class="control-label col-xs-3">Division</label>
                                <div class="col-xs-8">
                                    <input name="edit_division" id='edit_division' class="form-control" type="text" >
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                                    <button class="btn btn-info" type="submit">Edit</button>
                                </div>
                            </div>
                        </div>
                    </form>
              </div>
          </div>
      </div>
    </div>
  </div>



























  <script type="text/javascript">
    
    function upload(){

      let upload_file = document.getElementById("upload_file").value;
      let nama_c = document.getElementById("nama_c").value;
      let nama_file = document.getElementById("nama_file").value;
      let input_file = document.getElementById("input_file").value;
      let file_type = document.getElementById("file_type").value;
      

      project = $.ajax({
              data : { id_project : id_project },
              type:"POST",
              url: "<?php echo site_url('dokumen/getProject');?>",
              async: false
        }).responseText;
    
    }


    function edit_employe(id_type,type_file_e,file_ext_e){
      document.getElementById("id_type").value = id_type
      document.getElementById("type_file_e").value = type_file_e;
      document.getElementById("file_ext_e").value = file_ext_e;
    }

    function disableButton() {
      var btn = document.getElementById('btn_add_submit');
      btn.disabled = true;
      btn.innerText = 'Posting...';
  }
  </script>
  <!--END MODAL EDIT-->
    
      
      
