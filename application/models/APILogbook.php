<?php  
class APILogbook extends CI_Model{

    function __construct(){
        parent::__construct();
        $this->authorization = $this->config->item('core-authorization');
        $this->url = $this->config->item('url-corelogbook');

    }

    function getProject(){ 
        $url = $this->url."/project";
        $header = array (
            "Authorization: $this->authorization"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;

        
    }

    function Apipost($url, $body) {
        $header = array (
            "Authorization: $this->authorization",
            "content-type:application/json"
        );
        $arr=json_encode($body);
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $arr);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }


}; 

?>