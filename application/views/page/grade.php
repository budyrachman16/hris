<div class="page">

<!-- Header Page -->
  <!-- Header Page -->

  <!-- Content Page -->
  <div class="page-content">
    <h1 class="page-title">Grade List</h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo site_url('home')?>">Home</a></li>
          <li class="breadcrumb-item active">Grade</li>
        </ol>
    <!-- Body Page -->
    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title">
          <button id="add" class="btn btn-primary" onClick="modal_create()"  type="button" data-target="#modal_create"
            data-toggle="modal">
              <i class="icon md-plus-circle" aria-hidden="true"></i> Add Grade
          </button>
      </header>
      <div class="panel-body collapse show" id="exampleFooCollapsePanel">
        <table class="table table-bordered table-hover toggle-circle" id="exampleFooCollapse"
          data-paging="true" data-filtering="true" data-sorting="true">
          <thead>
            <tr>
              <th data-name="id" data-type="number" data-breakpoints="xs" width=50px>ID</th>
              <th data-name="firstName" width=200px>Name</th>
              <th data-name="lastName" width=200px>Grade</th>
              <th data-name="nik" width=200px>Level</th>
              <th data-name="divisi" width=200px>Action</th>            
            </tr>
          </thead>   
          <tbody> 
            <?php $i = 1; foreach ($grade as $g): ?>
                <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?= $g->name ?></td>
                    <td><?= $g->grade ?></td>
                    <td><?= $g->level ?></td>
                
                    <td class="actions">
                      <!-- Edit FIle -->
                      <a href="#" onClick="return edit_division('<?php echo $g->id_grade; ?>','<?php echo $g->name; ?>','<?php echo $g->grade;?>')"  class="btn btn-info btn-xs" data-toggle="modal" data-target="#modal_edit"><i class="icon md-edit"></i> Edit </a>
                      <!-- Delete -->
                      <!-- <a href="#" onclick='return del_confirm()' class="btn btn-danger btn-xs"><i class="icon md-delete"></i> Delete </a>  -->
                      <!-- <detail> -->
                      <!-- <a href="#" onclick='return del_confirm()' class="btn btn-success btn-xs"><i class="icon md-receipt"></i> Detail </a>  -->
                    </td>    
                </tr>                      
            <?php endforeach; ?>
          </tbody>          
        </table>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal_create" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <h3 class="modal-title" id="myModalLabel">Add Grade</h3>
          </div>
        <!-- <div class="modal-header"></div> -->
        <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('grade/postGrade')?>" onsubmit="disableButton()">
          <div class="modal-body">             
              <div class="form-group">
                  <label class="control-label col-xs-3">Grade</label>
                  <div class="col-xs-8">
                      <input name="add_grade" id='add_grade' class="form-control" type="text" >
                  </div>
              </div>
              <div class="form-group">
                  <label class="control-label col-xs-3">Name</label>
                  <div class="col-xs-8">
                      <input name="add_name" id='add_name' class="form-control" type="text" >
                  </div>
              </div>
              <div class="modal-footer">
                  <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                  <button class="btn btn-info" type="submit">Input</button>
              </div>
          </div>
        </form>
      </div>
    </div>
  </div>


  <div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <h3 class="modal-title" id="myModalLabel">Edit Grade</h3>
              </div>
                <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('grade/edit_grade')?>" onsubmit="disableButton()">
                    <div class="modal-body">
                        <div class="form-group">
                                <input name="id_grade" id='id_grade' class="form-control" type="text"  hidden>
                            <label class="control-label col-xs-3">Name</label>
                            <div class="col-xs-8">
                                <input name="edit_name" id='edit_name' class="form-control" type="text" >
                            </div>
                            <label class="control-label col-xs-3">Grade</label>
                            <div class="col-xs-8">
                                <input name="edit_gradeSS" id='edit_gradeSS' class="form-control" type="text" >
                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                                <button class="btn btn-info" type="submit">Edit</button>
                            </div>
                        </div>
                    </div>
                </form>
          </div>
      </div>
  </div>



</div>



























<script type="text/javascript">
  
  function upload(){

    let upload_file = document.getElementById("upload_file").value;
    let nama_c = document.getElementById("nama_c").value;
    let nama_file = document.getElementById("nama_file").value;
    let input_file = document.getElementById("input_file").value;
    let file_type = document.getElementById("file_type").value;
    

    project = $.ajax({
            data : { id_project : id_project },
            type:"POST",
            url: "<?php echo site_url('dokumen/getProject');?>",
            async: false
      }).responseText;
   
  }


  function edit_division(id_grade,name,grade,level){
    document.getElementById("id_grade").value = id_grade
    document.getElementById("edit_name").value = name;
    document.getElementById("edit_gradeSS").value = grade;
    // document.getElementById("edit_level").value = level;
  }

  function disableButton() {
    var btn = document.getElementById('btn_add_submit');
    btn.disabled = true;
    btn.innerText = 'Posting...';
}
</script>
<!--END MODAL EDIT-->
  
    
    
