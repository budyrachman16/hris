  <!-- Footer -->
  <footer class="site-footer">
      <div class="site-footer-legal">© 2019. All RIGHT RESERVED.</div>
      <div class="site-footer-right">
      <a href="http://www.gratika.co.id">PT Graha Informatika Nusantara</a>
      </div>
    </footer>

    <div class="modal fade modal-danger" id="warningmodal" aria-hidden="true" aria-labelledby="modalWarning" role="dialog" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title">Application Warning</h4>
          </div>
          <div class="modal-body">
            <h5><?php echo json_decode(json_encode($this->session->flashdata('message')))->message; ?></h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade modal-success" id="successmodal" aria-hidden="true" aria-labelledby="modalSuccess" role="dialog" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title">Application Success</h4>
          </div>
          <div class="modal-body">
            <h5><?php echo json_decode(json_encode($this->session->flashdata('message')))->message; ?></h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    
   <!-- Core  -->
    <script src="<?php echo base_url(). "global/" ?>vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/jquery/jquery.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/popper-js/umd/popper.min.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/bootstrap/bootstrap.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/animsition/animsition.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/waves/waves.js"></script>
    
    <!-- Plugins -->
    <script src="<?php echo base_url(). "global/" ?>vendor/switchery/switchery.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/intro-js/intro.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/screenfull/screenfull.js"></script>
    <script src="<?php echo base_url(). "global/" ?>vendor/slidepanel/jquery-slidePanel.js"></script>

        <script src="<?php echo base_url(). "global/" ?>vendor/jvectormap/jquery-jvectormap.min.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/matchheight/jquery.matchHeight-min.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/peity/jquery.peity.min.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/jquery-selective/jquery-selective.min.js"></script>

        <script src="<?php echo base_url(). "global/" ?>vendor/datatables.net/jquery.dataTables.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/datatables.net-fixedheader/dataTables.fixedHeader.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/datatables.net-fixedcolumns/dataTables.fixedColumns.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/datatables.net-rowgroup/dataTables.rowGroup.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/datatables.net-scroller/dataTables.scroller.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/datatables.net-responsive/dataTables.responsive.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/datatables.net-buttons/dataTables.buttons.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/datatables.net-buttons/buttons.html5.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/datatables.net-buttons/buttons.flash.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/datatables.net-buttons/buttons.print.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/datatables.net-buttons/buttons.colVis.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/datatables.net-buttons-bs4/buttons.bootstrap4.js"></script>


        <script src="<?php echo base_url(). "global/" ?>vendor/select2/select2.full.min.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/bootstrap-tokenfield/bootstrap-tokenfield.min.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/bootstrap-select/bootstrap-select.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/icheck/icheck.min.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/asrange/jquery-asRange.min.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/ionrangeslider/ion.rangeSlider.min.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/asspinner/jquery-asSpinner.min.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/clockpicker/bootstrap-clockpicker.min.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/ascolor/jquery-asColor.min.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/asgradient/jquery-asGradient.min.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/ascolorpicker/jquery-asColorPicker.min.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/bootstrap-maxlength/bootstrap-maxlength.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/jquery-knob/jquery.knob.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/bootstrap-touchspin/bootstrap-touchspin.min.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/jquery-labelauty/jquery-labelauty.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/timepicker/jquery.timepicker.min.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/datepair/datepair.min.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/datepair/jquery.datepair.min.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/jquery-strength/password_strength.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/jquery-strength/password_strength.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/jquery-strength/jquery-strength.min.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/multi-select/jquery.multi-select.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/typeahead-js/bloodhound.min.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/typeahead-js/typeahead.jquery.min.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/jquery-placeholder/jquery.placeholder.js"></script>

        <script src="<?php echo base_url(). "global/" ?>vendor/aspieprogress/jquery-asPieProgress.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/formvalidation/formValidation.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/jquery-wizard/jquery-wizard.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/asrange/jquery-asRange.min.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/bootbox/bootbox.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/formvalidation/framework/bootstrap.js"></script>

        <script src="<?php echo base_url(). "global/" ?>vendor/moment/moment.min.js"></script>
        <script src="<?php echo base_url(). "global/" ?>vendor/footable/footable.min.js"></script>

        
    
    
    <!-- Scripts -->
    <script src="<?php echo base_url(). "global/" ?>js/Component.js"></script>
    <script src="<?php echo base_url(). "global/" ?>js/Plugin.js"></script>
    <script src="<?php echo base_url(). "global/" ?>js/Base.js"></script>
    <script src="<?php echo base_url(). "global/" ?>js/Config.js"></script>
    
    <script src="<?php echo base_url(). "assets/" ?>js/Section/Menubar.js"></script>
    <script src="<?php echo base_url(). "assets/" ?>js/Section/Sidebar.js"></script>
    <script src="<?php echo base_url(). "assets/" ?>js/Section/PageAside.js"></script>
    <script src="<?php echo base_url(). "assets/" ?>js/Plugin/menu.js"></script>
    <script src="<?php echo base_url(). "global/" ?>js/Plugin/select2.js"></script>

    
    <!-- Config -->
    <script src="<?php echo base_url(). "global/" ?>js/config/colors.js"></script>
    <script src="<?php echo base_url(). "assets/" ?>js/config/tour.js"></script>
    <script>Config.set('assets', '../assets');</script>
    
    <!-- Page -->
    <script src="<?php echo base_url(). "assets/" ?>js/Site.js"></script>
    <script src="<?php echo base_url(). "global/" ?>js/Plugin/asscrollable.js"></script>
    <script src="<?php echo base_url(). "global/" ?>js/Plugin/slidepanel.js"></script>
    <script src="<?php echo base_url(). "global/" ?>js/Plugin/switchery.js"></script>
        <script src="<?php echo base_url(). "global/" ?>js/Plugin/jvectormap.js"></script>
        <script src="<?php echo base_url(). "global/" ?>js/Plugin/peity.js"></script>
        <script src="<?php echo base_url(). "assets/" ?>examples/js/uikit/icon.js"></script>
        <script src="<?php echo base_url(). "global/" ?>js/Plugin/bootstrap-select.js"></script>
        <script src="<?php echo base_url(). "global/" ?>js/Plugin/multi-select.js"></script>

        <script src="<?php echo base_url(). "global/" ?>js/Plugin/datatables.js"></script>
    
        <script src="<?php echo base_url(). "assets/" ?>examples/js/tables/datatable.js"></script>
        <script src="<?php echo base_url(). "assets/" ?>examples/js/uikit/icon.js"></script>

        <script src="<?php echo base_url(). "global/" ?>js/Plugin/jquery-wizard.js"></script>    
        <script src="<?php echo base_url(). "global/" ?>js/Plugin/matchheight.js"></script>
        <!-- <script src="<?php echo base_url(). "assets/" ?>examples/js/forms/wizard.js"></script> -->
        <script src="<?php echo base_url(). "assets/" ?>examples/js/tables/footable.js"></script>
        <script src="<?php echo base_url(). "global/" ?>js/Plugin/responsive-tabs.js"></script>
        <script src="<?php echo base_url(). "global/" ?>js/Plugin/tabs.js"></script>



        <?php if($this->session->flashdata('message') != NULL) {
          $status = json_decode(json_encode($this->session->flashdata('message')))->status;
        if($status == '1'){ ?>
            <script type="text/javascript"> $(window).on('load',function(){ $('#successmodal').modal('show'); }); </script>
        <?php } else { ?>
            <script type="text/javascript"> $(window).on('load',function(){ $('#warningmodal').modal('show'); }); </script>
        <?php }} ?>
