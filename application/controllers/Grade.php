<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    class Grade extends CI_Controller{

        function __construct(){

            parent::__construct();
            
            // $this->load->helper('url');
            $this->load->helper('form');
            $this->load->library('session');
            $this->load->model("user_model");
            $this->load->model("APIDocman");
            $this->load->model("APILogbook");
            $this->load->model("APIHris");        
            $this->load->model("APISimpok");        
            $this->url = $this->config->item('url');
            $this->url_hris = $this->config->item('url-hris');
            $this->load->model('Model');
            $this->session_key = $this->config->item('session-key');
    
        } 

        function index(){

            $id_company = $this->session->userdata('id');
            

            $grade = json_decode($this->APIDocman->ApigetTipe($this->url_hris."/grade/level/$id_company"))->data;



            
            $data["id_company"] = $id_company;
            $data["grade"] = $grade;


            $key = $this->session->userdata('log');

            if ($key =="loveyou"){
                $this->load->view('frame/a_header');
                $this->load->view('frame/b_nav');
                $this->load->view('page/grade', $data);
                $this->load->view('frame/d_footer');    
            }else{
                $this->load->view('page/login');
            }    
        }


        function postGrade(){

            $id_company = $this->session->userdata('id');
            
      
            
            $name = $this->input->post('add_name');
            $grade = $this->input->post('add_grade');
      
            
            $body = array(
              "name" => $name,
              "grade" => $grade,
        
            );
      
      
            // var_dump($body);
            // exit;
            $respon = json_decode($this->APIHris->ApiPost($this->url_hris."/grade", $body));
      
            var_dump($respon);
            exit;
      
            if($respon==null){
              $array=array('status' => '0','message' => 'API not response . please contact administrator');
              $this->session->set_flashdata('message', $array);
            } else if($respon->status =='1'){
              $array=array('status' => '1','message' => 'Data has been Added.. ');
              $this->session->set_flashdata('message', $array);
            } else {
              $array=array('status' => '0','message' => $respon->data);
              $this->session->set_flashdata('message', $array);
            }
            redirect('division');
          }


          function edit_grade(){
            $id_company = $this->session->userdata('id');
           $id_grade = $this->input->post('id_grade');
            $name = $this->input->post('edit_name');
            $grade = $this->input->post('edit_gradeSS');
    
    
      
      
            $body = array(
                "name" =>  $name,
                "grade" =>  $grade,
            );
      
          
          
    
    

           
            $respon = json_decode($this->APIHris->Apiput($this->url_hris."/grade/$id_grade", $body));
            
            // var_dump($respon);
            // exit;
          
           
          
      
            if($respon==null){
              $array=array('status' => '0','message' => 'API not response . please contact administrator');
              $this->session->set_flashdata('message', $array);
            } else if($respon->status =='1'){
              $array=array('status' => '1','message' => 'Data has been Edited.. ');
              $this->session->set_flashdata('message', $array);
            } else {
              $array=array('status' => '0','message' => $respon->data);
              $this->session->set_flashdata('message', $array);
            }
            redirect('grade');
          }

            
      
}