
<div class="page">
    <div class="page-content">
        <!-- Example Panel With Tool -->
        <div class="panel">
            <div class="panel-body">
                <form data-parsley-validate class="form-horizontal form-label-left" method="post"
                    onSubmit="disableButton()" enctype="multipart/form-data"
                    action="<?php echo site_url(). "/employe/saveedit" ?>">
                    <div class="modal-body">
                        <div class="pearls row">
                            <div id="pearl-project" class="pearl current col-6">
                                <div class="pearl-icon"><i class="icon md-account" aria-hidden="true"></i></div>
                                <span class="pearl-title">Personal Data</span>
                            </div>
                            <div id="pearl-item" class="pearl col-6">
                                <div class="pearl-icon"><i class="icon md-city" aria-hidden="true"></i></div>
                                <span class="pearl-title">Company Data</span>
                            </div>
                        </div>
                        <div id="wizard-content" class="wizard-content">
                            <div style="display:show" class="wizard-pane active" id="wizard-kesatu" role="tabpanel">
                                <div class="row">
                                    <input type="text" class="form-control validate_char" id="id_employe"
                                        value="<?php echo $edit[0]->id_employee; ?>" name="id_employe"
                                        placeHolder="type your purpose" required hidden />
                                    <div id="div_project" class="col-lg-6 form-group row">
                                        <label class="col-lg-3 col-form-label"><b>First Name : </b></label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control validate_char" id="edit_firstName"
                                                value="<?php echo $edit[0]->first_name; ?>" name="edit_firstName"
                                                placeHolder="type your purpose" required />
                                            </div>
                                    </div>
                                    <div class="col-lg-6 form-group row">
                                        <label class="col-lg-3 col-form-label"><b>Alamat : </b></label>
                                        <div class="col-lg-9">
                                            <textarea class="form-control validate_char" id="edit_alamat" name="edit_alamat"
                                                placeholder="type your note"
                                                required><?php echo $edit[0]->alamat; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 form-group row">
                                        <label class="col-lg-3 col-form-label">Last Name : </label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control validate_char" id="edit_last_name"
                                                value="<?php echo $edit[0]->last_name; ?>" name="edit_last_name"
                                                placeHolder="type your purpose" required />
                                        </div>
                                    </div>
                                    <div class="col-lg-6 form-group row">
                                        <label class="col-lg-3 control-label"><b>Provinsi : </b></label>
                                        <div class="col-lg-9">
                                            <select id="add_provinsi" name="add_provinsi" class="form-control"
                                                data-plugin="selectpicker" data-live-search="true" onchange="select_radio_project(event)">
                                                    <?php foreach ($provinsi as $provinsi): 
                                                        if ($edit[0]->prov != null &&  $provinsi->id == $edit[0]->prov->id){?>
                                                            <option selected value="<?php echo $provinsi->id; ?>"><?php echo $provinsi->nama; ?></option>
                                                        <?php } else {?>
                                                            <option value="<?php echo $provinsi->id; ?>"><?php echo $provinsi->nama; ?></option>
                                                        <?php }
                                                        ?>
                                                    <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 form-group row">
                                        <label class="col-lg-3 col-form-label">NIK : </label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control validate_char" id="edit_nik"
                                                value="<?php echo $edit[0]->nik; ?>" name="edit_nik"
                                                placeHolder="type your purpose" required />
                                        </div>                       
                                    </div>                      
                                    <div class="col-lg-6 form-group row">
                                        <label class="col-lg-3 col-form-label">Kabupaten : </label>
                                        <div class="col-lg-9">
                                            <select id="add_kabupaten" name="add_kabupaten" class="form-control"
                                                data-plugin="selectpicker" data-live-search="true" onchange="select_radio_kabupaten(event)">
                                                <?php foreach ($kabupaten as $edit_kabupaten): 
                                                    if ($edit[0]->kab != null && $edit_kabupaten->id == $edit[0]->kab){?>
                                                        <option selected value="<?php echo $edit_kabupaten->id; ?>"><?php echo $edit_kabupaten->nama; ?></option>
                                                    <?php } else {?>
                                                        <option value="<?php echo $edit_kabupaten->id; ?>"><?php echo $edit_kabupaten->nama; ?></option>
                                                    <?php }
                                                    ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 form-group row">
                                        <label class="col-lg-3 col-form-label">NPWP : </label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control validate_char" id="edit_npwp"
                                                value="<?php echo $edit[0]->npwp; ?>" name="edit_npwp"
                                                placeHolder="type your purpose" required />
                                        </div>
                                    </div>
                                    <div class="col-lg-6 form-group row">
                                        <label class="col-lg-3 col-form-label">Kecamatan : </label>
                                        <div class="col-lg-9">
                                            <select id="add_kecamatan" name="add_kecamatan" class="form-control"
                                                data-plugin="selectpicker" data-live-search="true" onchange="select_radio_kabupaten(event)">
                                                <?php foreach ($kecamatan as $edit_kecamatan): 
                                                    if ($edit[0]->kec != null && $edit_kecamatan->id == $edit[0]->kec){?>
                                                        <option selected value="<?php echo $edit_kecamatan->id; ?>"><?php echo $edit_kecamatan->nama; ?></option>
                                                    <?php } else {?>
                                                        <option value="<?php echo $edit_kecamatan->id; ?>"><?php echo $edit_kecamatan->nama; ?></option>
                                                    <?php }
                                                    ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 form-group row">
                                        <label class="col-lg-3 col-form-label">Phone : </label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control validate_char" id="edit_phone"
                                                value="<?php echo $edit[0]->telephone; ?>" name="edit_phone"
                                                placeHolder="type your purpose" required />
                                        </div>
                                    </div>
                                    <div class="col-lg-6 form-group row">
                                        <label class="col-lg-3 col-form-label">National : </label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control validate_char" id="edit_nation"
                                                value="<?php echo $edit[0]->kenegaraan; ?>" name="edit_nation"
                                                placeHolder="type your purpose" required />
                                        </div>
                                    </div>
                                    <div class="col-lg-6 form-group row">
                                        <label class="col-lg-3 col-form-label">Email : </label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control validate_char" id="edit_mail"
                                                value="<?php echo $edit[0]->email; ?>" name="edit_mail"
                                                placeHolder="type your purpose" required />
                                        </div>
                                    </div>        
                                </div>
                            </div>            
                            <div style="display:none" class="wizard-akta" id="wizard-kedua" role="tabpanel">
                                <div class="row">
                                    <div id="doc_type" class="col-lg-6 form-group row">
                                        <label class="col-lg-3 col-form-label"><b>Work Hour : </b></label>
                                        <div class="col-lg-9">
                                            <select id="add_work_hour" name="add_work_hour" class="form-control"
                                                data-plugin="selectpicker" data-live-search="true" onchange="select_radio_project(event)">
                                                <?php foreach ($work_hour as $edit_workhour): 
                                                    if($edit[0]->work_hour->id_hour != null && $edit_workhour->id_hour == $edit[0]->work_hour->info){?>
                                                        <option selected value="<?php echo $edit_workhour->id_hour; ?>"><?php echo $edit_workhour->info; ?></option>
                                                    <?php } else {?>
                                                        <option value="<?php echo $edit_workhour->id_hour; ?>"><?php echo $edit_workhour->info; ?></option>
                                                    <?php }
                                                ?>
                                                <?php endforeach; ?>
                                            </select>                        
                                        </div>
                                    </div>     
                                </div>  
                                <div class="row">   
                                    <div id="div_nomor" class="col-lg-6 form-group row" style="display:show">
                                        <label class="col-lg-3 col-form-label">Divisi </label>
                                        <div class="col-lg-9">
                                            <select id="add_divisi" name="add_divisi" class="form-control"
                                                data-plugin="selectpicker" data-live-search="true" onchange="select_radio_project(event)">
                                                    <?php foreach ($divisi as $edit_divisi):
                                                        if($edit[0]->divisi->id_divisi != null && $edit_divisi->id_divisi == $edit[0]->divisi->name){?>
                                                            <option selected value="<?php echo $edit_divisi->id_divisi; ?>"><?php echo $edit_divisi->name; ?></option>
                                                        <?php } else {?>
                                                            <option value="<?php echo $edit_divisi->id_divisi; ?>"><?php echo $edit_divisi->name; ?></option>
                                                        <?php }
                                                        ?>
                                                        <option value="<?php echo $edit_divisi->id_divisi; ?>"><?php echo $edit_divisi->name; ?></option>
                                                    <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>                              
                                </div> 
                                <div class="row">
                                    <div id="doc_type" class="col-lg-6 form-group row">
                                        <label class="col-lg-3 col-form-label"><b>Level : </b></label>
                                        <div class="col-lg-9">
                                            <select id="add_level" name="add_level" class="form-control"
                                                data-plugin="selectpicker" data-live-search="true" onchange="select_radio_project(event)">
                                                    <?php foreach ($level as $edit_level): 
                                                        if($edit[0]->job_level->id_level != null && $edit_level->id_level == $edit[0]->job_level->id_level){?>
                                                            <option value="<?php echo $edit_level->id_level; ?>"><?php echo $edit_level->name; ?></option>
                                                        <?php } else {?>
                                                            <option value="<?php echo $edit_level->id_level; ?>"><?php echo $edit_level->name; ?></option>
                                                        <?php }
                                                        ?>
                                                    <?php endforeach; ?>
                                            </select>                          
                                        </div>
                                    </div>     
                                </div>  
                                <div class="row">   
                                    <div id="div_nomor" class="col-lg-6 form-group row" style="display:show">
                                        <label class="col-lg-3 col-form-label">Role </label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control validate_char" id="edit_role"
                                                value="<?php echo $edit[0]->role; ?>" name="edit_role"
                                                placeHolder="type your purpose" required />
                                        </div>
                                    </div>
                                </div>    
                                <div class="row">   
                                    <div id="div_nomor" class="col-lg-6 form-group row" style="display:show">
                                        <label class="col-lg-3 col-form-label">Status </label>
                                        <div class="col-lg-9">
                                            <select id="add_status" name="add_status" class="form-control"
                                                data-plugin="selectpicker" data-live-search="true" value="<?php echo $edit[0]->status; ?>">
                                                <option Value="0">Akktif</option>
                                                <option Value="1">Tidak Aktif</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>    
                                <div class="row">   
                                    <div id="div_nomor" class="col-lg-6 form-group row" style="display:show">
                                        <label class="col-lg-3 col-form-label">Gaji </label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control validate_char" id="edit_gaji"
                                                value="<?php if ($edit[0]->gaji == null || $edit[0]->gaji == ""){
                                                        echo ("");
                                                } else {
                                                    echo $edit[0]->gaji->nilai;
                                                }
                                                
                                                ?>" name="edit_gaji"
                                                placeHolder="type your purpose" required />
                                        </div>
                                    </div>
                                </div>    
                            </div>
                        </div>
                        <div id="alert-add-rab" style="display:none" class="alert dark alert-danger alert-dismissible"
                            role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <i class="icon wb-alert-circle" aria-hidden="true"></i> Alert, Semua field di isi.
                        </div>
                        <div id="alert-confirm" style="display:none" class="alert dark alert-danger alert-dismissible"
                            role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <i class="icon wb-alert-circle" aria-hidden="true"></i> Alert, Sesuaikan nilai item yang
                            dibutuhkan dengan nilai COA yang digunakan. Jika tidak memiliki saldo pada bulan ini, ajukan
                            RRA pada buton biru dibawah ini.
                        </div>
                        <hr />
                        <div class="wizard-buttons">
                            <button id="btn_add_back" type="button" class="btn btn-primary btn-outline disabled"
                                onClick="back()">Back</button>
                            <button id="btn_add_next" type="button" class="btn btn-primary btn-outline float-right"
                                onClick="next()">Next</button>
                            <button style="display:none" id="btn_add_submit" type="submit"
                                class="btn btn-info btn-outline float-right">Update
                            </button>
                        </div>
                    </div>
                </form>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<script>


// function disableButton() {
// $('.custom_datepicker').attr("disabled", false);
// $('.custom_timepicker').attr("disabled", false);
// $('.custom_select').attr("disabled", false)
// var btn = document.getElementById('btn_add_submit');
// var btn_rel = document.getElementById('btn_relation_submit');
// btn.disabled = true;
// btn.innerText = 'Posting...';
// btn_rel.disabled = true;
// btn_rel.innerText = 'Posting...';
// }

function select_id_project(e) {
$("#add_coa_nmr0").find("option").remove();
$("#add_coa_nmr_rra").find("option").remove();

//GET COAS DARI ID PROJECT
coas = $.ajax({
    data: {
        id_project: e.target.value
    },
    type: "POST",
    url: "<?php echo site_url('rab/getCoasAPI');?>",
    success: function(msg) {
        let month = new Date();
        // console.log("month: " + month.getMonth());
        // console.log("[coas]: " + msg);
        let dataC = JSON.parse(msg);
        let dumy_coa = "";

        let flag_pendapatan = 0;
        for (let i = 0; i < dataC.length; i++) {
            const element = dataC[i];
            if (element.id_type == '1') {
                flag_pendapatan = 12;
                continue;
            }
            let no_coa = element.no_coa;
            let name = element.no_coa + " | " + element.name_type;
            if (dumy_coa != no_coa) {
                $('#add_coa_nmr0').append("<option value='" + no_coa + "'>" + name + "</option>");
                $('#add_coa_nmr_rra').append("<option value='" + no_coa + "'>" + name + "</option>");
                dumy_coa = no_coa;
            }
        }
        $('#add_coa_nmr0').selectpicker('refresh');
        $('#add_coa_nmr_rra').selectpicker('refresh');
        document.getElementById("add_coa_harga0").value = convertToRupiah(dataC[flag_pendapatan + (month
                .getMonth())]
            .saldo_coa);
        document.getElementById("add_rra_saldo").value = convertToRupiah(dataC[flag_pendapatan + month
                .getMonth() + 1]
            .saldo_coa);
    }
});
}

function next() {
    let value = parseInt(localStorage.getItem('create-wizard-rab'));
    console.log("flag wizard next: " + value);
    if (value == 0) {
        let product_title = document.getElementById("edit_firstName").value;
        let judul_product = document.getElementById("edit_alamat").value;
        let product_name = document.getElementById("edit_last_name").value;
        let product_info = document.getElementById("add_provinsi").value;
        let product_usability = document.getElementById("edit_nik").value;
        let product_kegunaan = document.getElementById("add_kabupaten").value;
        let prouct_data = document.getElementById("edit_npwp").value;
        let product_dataIn = document.getElementById("add_kecamatan").value;
        let product_prevention = document.getElementById("edit_phone").value;
        let product_pemeliharaan = document.getElementById("edit_nation").value;
        let product_values = document.getElementById("edit_mail").value;
        let product_nilai = document.getElementById("add_work_hour").value;
        let product_diskon = document.getElementById("add_divisi").value;
        let menu_id = document.getElementById("add_level").value;
        let sub_kategori = document.getElementById("edit_role").value;
        let product_related = document.getElementById("add_status").value;
        let variant_value = document.getElementById("edit_gaji").value;

        console.log (first_name);
        console.log (alamat);
        console.log (last_name);
        console.log (provinsi);
        console.log (nik);
        console.log (kabupaten);
        console.log (npwp);
        console.log (kecamatan);
        console.log (telephone);
        console.log (national);
        console.log (email);
        // console.log (work_hour);
        // console.log (divisi);
        // console.log (level);
        // console.log (role);
        // console.log (status);
        // console.log (gaji);
    

        if (first_name == "" || alamat == "" || last_name == "" || provinsi == "" ||
        nik == "" || kabupaten == "" || npwp == "" || kecamatan == "" || telephone == "" || national == "" || email == "") {

            alert("Isi data dengan benar");

            // $("#alert-add-rab").show();
            // $("#pearl-project").attr('class', 'pearl current col-6 active error');
            // $("#wizard-kesatu").show();
            // $("#wizard-kedua").hide();
            // $("#btn_add_back").attr('class', 'btn btn-primary btn-outline disabled');
            // $("#btn_add_submit").click();

        } else {
            console.log ("aaaaaaa");


            
            $("#alert-add_docman").hide();
            $("#pearl-project").attr('class', 'pearl current col-6');
            $("#pearl-item").attr('class', 'pearl current col-6');
            $("#wizard-kesatu").hide();
            $("#wizard-kedua").show();
            $("#btn_add_back").attr('class', 'btn btn-primary btn-outline');
            $("#btn_add_submit").show();
            $("#btn_add_next").hide();
        }

    } else if (value == 0) {

        console.log ("aaaa");


        let work_hour  = document.getElementById("add_work_hour").value;
        let divisi = document.getElementById("add_divisi").value;
        let level = document.getElementById("add_level").value;
        let gaji = document.getElementById("edit_gaji").value;
        let status = document.getElementById("add_status").value;
        let role = document.getElementById("edit_role").value;

        $("#alert-add_docman").hide();
        $("#pearl-project").attr('class', 'pearl current col-6');
        $("#pearl-item").attr('class', 'pearl current col-6');
        $("#wizard-kesatu").hide();
        $("#wizard-kedua").show();
        $("#btn_add_submit").show();
        $("#btn_add_next").hide();
        $("#btn_add_back").attr('class', 'btn btn-primary btn-outline');
    }
     
  
}


function back() {
    let value = parseInt(localStorage.getItem('create-wizard-rab'));
        console.log("flag wizard back: " + value);
        localStorage.setItem('create-wizard-rab', value);
        if (value == 0) { //form project
            $("#pearl-project").attr('class', 'pearl current col-6');
            $("#pearl-item").attr('class', 'pearl col-6');
            $("#pearl-coa").attr('class', 'pearl col-6');
            $("#pearl-confirm").attr('class', 'pearl col-6');
            $("#wizard-kesatu").show();
            $("#wizard-coa").hide();
            $("#wizard-kedua").hide();
            $("#wizard-confirm").hide();
            $("#btn_add_next").show();
            $("#btn_add_submit").hide();
            $("#btn_add_back").attr('class', 'btn btn-primary btn-outline disabled');
            $("#alert-add_docman").hide();

        } else if (value == 1) { //form item
            $("#pearl-project").attr('class', 'pearl current col-6');
            $("#pearl-item").attr('class', 'pearl current col-6');
            $("#pearl-coa").attr('class', 'pearl col-6');
            $("#pearl-confirm").attr('class', 'pearl col-6');
            $("#wizard-kesatu").hide();
            $("#wizard-item").show();
            $("#wizard-coa").hide();
            // $("#btn_add_next").hide();
            $("#btn_add_submit").show();
            $("#btn_add_back").attr('class', 'btn btn-primary btn-outline');
            $("#alert-add_docman").hide();

        } else if (value == 2) { //form coa
            $("#pearl-project").attr('class', 'pearl current col-6');
            $("#pearl-item").attr('class', 'pearl current col-6');
            $("#pearl-coa").attr('class', 'pearl current col-6');
            $("#pearl-confirm").attr('class', 'pearl col-6');
            $("#wizard-kesatu").hide();
            $("#wizard-item").hide();
            $("#wizard-coa").show();
            $("#wizard-confirm").hide();
            // $("#btn_add_next").hide();
            $("#btn_add_submit").show();
            $("#btn_add_back").attr('class', 'btn btn-primary btn-outline');
            $("#alert-add_docman").hide();
            $("#btn_rra").hide();

        } else {
            console.log("tidak ada");
        }
}


$(document).ready(function() {
// console.log("DOCUMENT READY FUNCTION");
localStorage.setItem('create-wizard-rab', 0);
// console.log("flag document ready: ");
});






function select_radio_project(e) {
let status = e.target.value;
alert(status);

kabupaten = $.ajax({
data: {
    id : status
    },
    type: "POST",
    url: "<?php echo site_url('Inputemploye/getKabupaten');?>",
    async: false
}).responseText;


let dataC = JSON.parse(kabupaten);
for (let i = 0; i < dataC.length; i++) {
    const element = dataC[i];
    let name = element.nama;
    let value = element.id;
    $('#add_kabupaten').append("<option value='" + value + "'>" + name + 
        " </option>");
}

$('#add_kabupaten').selectpicker('refresh');
    console.log(kabupaten);
} 




function select_radio_kabupaten(e) {
let statuss = e.target.value;
alert(statuss);

kecamatan = $.ajax({
    data: {
        id : statuss
        },
        type: "POST",
        url: "<?php echo site_url('Inputemploye/getKecamatan');?>",
        async: false
  }).responseText;
  console.log(kecamatan);



  let dataC = JSON.parse(kecamatan);
    for (let i = 0; i < dataC.length; i++) {
        const element = dataC[i];
        let name = element.nama;
        let value = element.id;
        $('#add_kecamatan').append("<option value='" + value + "'>" + name + 
            " </option>");
    }
    
    $('#add_kecamatan').selectpicker('refresh');
     console.log(kecamatan);
} 



</script>
