<?php  
class APIHris extends CI_Model{


    function __construct(){
        parent::__construct();
        $this->authorization = $this->config->item('core-authorization');
        // $this->url  = $this->config->item('url-corelogbook');
        $this->url = $this->config->item('url-hris');
        

    }


    function getLogin($url, $body){

        // var_dump($body);
        // exit;

        $header = array (
            "Content-type: application/json"
        );
        $arr=json_encode($body);
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $arr);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;

        
      }

    function Apiget($url){

        // var_dump($url);
        // exit;
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJncmF0aWthIiwidXNybiI6ImdyYXRpa2EiLCJwd2QiOiI4MjdjY2IwZWVhOGE3MDZjNGMzNGExNjg5MWY4NGU3YiIsImlhdCI6MTU5NTIxMjIzNSwiZXhwIjoxNjI2NzQ4MjM1fQ.jpHnSXoheEv-5UfEwSq8TbPSo4F-8Q9yZtFXkXxYFnc',
            "Content-type: application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }


    function ApiPost($url, $body) {
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJncmF0aWthIiwidXNybiI6ImdyYXRpa2EiLCJwd2QiOiI4MjdjY2IwZWVhOGE3MDZjNGMzNGExNjg5MWY4NGU3YiIsImlhdCI6MTU5NTIxMjIzNSwiZXhwIjoxNjI2NzQ4MjM1fQ.jpHnSXoheEv-5UfEwSq8TbPSo4F-8Q9yZtFXkXxYFnc',
            "Content-type: application/json"
        );
        $arr=json_encode($body);
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $arr);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }


    function Apiput($url, $body) {
        $header = array (
            'appsession:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzdmMtand0ZW5jb2RlZCIsImlkIjoxLCJ1c3IiOiJncmF0aWthIiwidXNybiI6ImdyYXRpa2EiLCJwd2QiOiI4MjdjY2IwZWVhOGE3MDZjNGMzNGExNjg5MWY4NGU3YiIsImlhdCI6MTU5NTIxMjIzNSwiZXhwIjoxNjI2NzQ4MjM1fQ.jpHnSXoheEv-5UfEwSq8TbPSo4F-8Q9yZtFXkXxYFnc',
            "Content-type: application/json"
        );
        $arr = json_encode($body); 
        $curl = curl_init($url); 
        curl_setopt($curl, CURLOPT_HEADER, false); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT"); 
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header); 
        curl_setopt($curl, CURLOPT_POST, true); 
        curl_setopt($curl, CURLOPT_POSTFIELDS, $arr); 
        $json_response = curl_exec($curl); 
        curl_close($curl); 
        return $json_response; 
    }




}; 

?>